# lhtd-admin-project
商品テンプレート(LIXIL製品のQRコード管理システム)の管理画面Webフロントエンド用リポジトリ  

## Link  
### 関連リポジトリ
* [API用リポジトリ(lhtd-api-project)](https://github.com/lixiljp/lhtd-api-project)
* [管理Webフロントエンド用リポジトリ(lhtd-admin-project)](https://github.com/lixiljp/lhtd-admin-project)
* [ユーザーWebフロントエンド用リポジトリ(lhtd-project)](https://github.com/lixiljp/lhtd-project)

### アーキテクチャ
* [アーキテクチャ図・概要](https://lixil.facebook.com/groups/200432660590568/200577673909400/)

### 言語/フレームワーク  
* HTML+CSS+Javascript / React.js

### サーバー・プラットフォーム
#### 本番環境
* [Google Cloud Platform(PJ:lhtd-admin-project)](https://console.cloud.google.com/home/dashboard?project=lhtd-admin-project)
* [Firebase(PJ:lhtd-admin-project)](https://console.firebase.google.com/u/0/project/lhtd-admin-project/overview?hl=ja)

#### 開発環境
* [Google Cloud Platform(PJ:dtc-lixil)](https://console.cloud.google.com/home/dashboard?project=dtc-lixil)
* [Firebase(PJ:dtc-lixil)](https://console.firebase.google.com/u/0/project/dtc-lixil/overview?hl=ja)

## デプロイ手順
* [開発環境](docs/deploy-manual.dev.md)
* [本番環境](docs/deploy-manual.prod.md)

## 事前準備
### yarn をインストールする
https://yarnpkg.com/en/docs/getting-started

### Firebase CLI を設定する
```bash
# Firebase CLIをインストールする
yarn install -g firebase-tools
# Firebaseにログインする
firebase login
```

### 設定ファイル
[設定ファイル](src/config/constant.js)でそれぞれの環境(production, staging, development)の設定を確認する
