import { BACKEND_URL } from "config/constants";
import { DEFAULT_PAGINATION_LIMIT } from "config/constants";
import { get, del, post, put } from "utils/fetch";

import { push } from "react-router-redux";
import store from "store";
// Actions
export const types = {
  SET_LOT_NO_FLAG: "QR_CREATE/SET_LOT_NO_FLAG",
  SET_ITEM_CODE_FLAG: "QR_CREATE/SET_ITEM_CODE_FLAG",
  SET_SEARCH_LIST: "ITEM/SET_SEARCH_LIST",
  SET_CURRENT_TABLE_PAGE: "ITEM/SET_CURRENT_TABLE_PAGE",
  SEARCH_ASYNC: "ITEM/SEARCH_ASYNC",
  DELETE_ONE_ASYNC: "ITEM/DELETE_ONE_ASYNC",
  SET_CURRENT_RESULT_SEARCH_SUGGESTION:
    "ITEM/SET_CURRENT_RESULT_SEARCH_SUGGESTION",
  SET_SELECTED_SUGGESTION: "ITEM_CREATE/SET_SELECTED_SUGGESTION",
  SET_SEARCH_STATUS: "ITEM_CREATE/SET_SEARCH_STATUS",
  SET_CURRENT_KEYWORD: "ITEFM/SET_CURRENT_KEYWORD",
  SET_ERROR: "ITEM_CREATE/SET_ERROR",
  SET_STATUS: "ITEM_CREATE/SET_STATUS",
  SET_CODE: "ITEM_CREATE/SET_CODE",
  SET_ERROR_CODE: "ITEM_CREATE/SET_ERROR_CODE",
  SET_TECH_CODE: "ITEM_CREATE/SET_TECH_CODE",
  SET_CONSUMER_NAME: "ITEM_CREATE/SET_CONSUMER_NAME",
  SET_NAME: "ITEM_CREATE/SET_NAME",
  CLEAR_ALL: "ITEM_CREATE/CLEAR_ALL",
  SET_ITEM: "ITEM_CREATE/SET_ITEM",
  SET_ROW_STATUS: "ITEM_CREATE/SET_ROW_STATUS",
  SET_MODAL_ISOPEN: "ITEM_CREATE/SET_MODAL_ISOPEN"
};

// Enum
export const status = {
  LOADING: "LOADING",
  SUCCESS: "SUCCESS",
  ERROR: "ERROR",
  STALE: "STALE"
};
//Reducer
export const initialState = {
  modalIsOpen: false,
  searchStatus: null,
  status: null,
  total: 0,
  list: [],
  showLotNoFlag: true,
  showCodeFlag: true,
  currentTablePage: 1,
  currentSearchKeyword: "",
  productId: -1,
  consumerName: "",
  currentResultSearchSuggestion: [],
  error: {
    code: null,
    codeMsg: "",
    techCode: "",
    consumerName: "",
    name: "",
    ERR: null
  },
  code: "",
  techCode: 1,
  name: "",
  rowStatus: null
};

const Item = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SEARCH_LIST:
      return {
        ...state,
        status: action.payload.status,
        total: action.payload.total,
        list: action.payload.list || []
      };
    case types.SET_MODAL_ISOPEN:
      return {
        ...state,
        modalIsOpen: action.payload.modalIsOpen,
        status: action.payload.status
      };
    case types.SET_LOT_NO_FLAG:
      return { ...state, showLotNoFlag: action.payload.showLotNoFlag };
    case types.SET_ITEM_CODE_FLAG:
      return { ...state, showCodeFlag: action.payload.showCodeFlag };
    case types.SET_SELECTED_SUGGESTION:
      return {
        ...state,
        productId: action.payload.productId || -1,
        consumerName: action.payload.consumerName || ""
      };
    case types.SET_CURRENT_RESULT_SEARCH_SUGGESTION:
      let list = action.payload.list;
      if (list) {
        return {
          ...state,
          currentResultSearchSuggestion: list,
          error: { ...state.error, consumerName: "" }
        };
      } else {
        return { ...state, currentResultSearchSuggestion: [] };
      }

    case types.CLEAR_CURRENT_RESULT_SEARCH_SUGGESTION:
      return { ...state, currentResultSearchSuggestion: [] };
    case types.SET_CURRENT_TABLE_PAGE:
      return { ...state, currentTablePage: action.payload.currentTablePage };
    case types.SET_SEARCH_STATUS:
      return { ...state, searchStatus: action.payload.searchStatus };
    case types.SET_ROW_STATUS:
      return { ...state, rowStatus: action.payload.rowStatus };
    case types.SET_ERROR:
      return { ...state, error: action.payload.error || state.error };
    case types.SET_ERROR_CODE:
      return { ...state, error: { code: action.payload.error.error.code } };
    case types.SET_STATUS:
      return { ...state, status: action.payload.status };
    case types.SET_CURRENT_KEYWORD:
      return {
        ...state,
        currentSearchKeyword: action.payload.currentSearchKeyword
      };
    case types.SET_CODE:
      return { ...state, code: action.payload.code };
    case types.SET_TECH_CODE:
      return { ...state, techCode: action.payload.techCode };
    case types.SET_CONSUMER_NAME:
      return { ...state, consumerName: action.payload.consumerName };
    case types.SET_NAME:
      return { ...state, name: action.payload.name };
    case types.CLEAR_ALL:
      return {
        ...state,
        error: {
          codeMsg: "",
          techCode: "",
          consumerName: "",
          name: ""
        },
        status: null,
        code: "",
        techCode: 1,
        consumerName: "",
        currentResultSearchSuggestion: [],
        name: "",
        productId: -1
      };
    case types.SET_ITEM:
      let item = action.payload.item;
      if (item) {
        return {
          ...state,
          status: null,
          code: item.code,
          techCode: item.techCode,
          consumerName: item.Product ? item.Product.consumerName : "",
          showCodeFlag: item.showCodeFlag,
          showLotNoFlag: item.showLotNoFlag,
          name: item.name,
          productId: item.Product ? item.Product.id : ""
        };
      } else {
        return { ...state, status: status.LOADING };
      }
    default:
      return state;
  }
};

export default Item;

function searchAsync(
  keyword = "",
  offset = 0,
  limit = DEFAULT_PAGINATION_LIMIT
) {
  return dispatch => {
    if (!keyword) keyword = "";
    else keyword = encodeURIComponent(keyword);
    dispatch(actions.setStatus(status.LOADING));
    const url = `${BACKEND_URL}/items?limit=${limit}&offset=${offset}&keyword=${keyword}`;
    get(url)
      .then(res => {
        res.json().then(result => {
          if (res.status === 200) {
            dispatch(
              actions.setSearchList(status.SUCCESS, result.count, result.rows)
            );
            localStorage.setItem("item-count", result.count);
          } else {
            dispatch(actions.setSearchList(status.ERROR));
            localStorage.removeItem("item-count");
          }
        });
      })
      .catch(err => {
        dispatch(actions.setSearchList(status.ERROR));
        localStorage.removeItem("item-count");
      });
  };
}

function deleteOneAsync(id) {
  return dispatch => {
    dispatch(actions.setRowStatus(status.LOADING));
    let url = `${BACKEND_URL}/items/${id}`;
    del(url)
      .then(res => {
        if (res.status === 200) {
          dispatch(actions.setRowStatus(status.SUCCESS));
        } else {
          dispatch(actions.setRowStatus(status.ERROR));
        }
      })
      .catch(err => {
        dispatch(actions.setRowStatus(status.ERROR));
      });
  };
}

function searchSuggestionAsync(searchValue) {
  return dispatch => {
    if (searchValue.length > 0) {
      dispatch(actions.setSearchStatus(status.LOADING));
      const url = `${BACKEND_URL}/items?limit=5&offset=0&keyword=${searchValue}`;
      get(url)
        .then(res => {
          res.json().then(result => {
            let dataBuff = [];
            result.rows.map(item => {
              return dataBuff.push({
                id: item.id,
                title: item.code
              });
            });
            dispatch(actions.setSearchStatus(status.SUCCESS));
            dispatch(actions.setCurrentResultSearchSuggestion(dataBuff));
          });
        })
        .catch(err => {
          dispatch(actions.setSearchStatus(status.ERROR));
        });
    } else {
      dispatch(actions.setSearchStatus(status.ERROR));
      return dispatch(actions.setCurrentResultSearchSuggestion([]));
    }
  };
}
function searchProductSuggestionAsync(searchValue, currentStatus) {
  return dispatch => {
    if (
      searchValue != null &&
      searchValue.length > 0 &&
      currentStatus !== "LOADING"
    ) {
      dispatch(actions.setSearchStatus(status.LOADING));
      const url = `${BACKEND_URL}/products?limit=5&offset=0&keyword=${searchValue}`;
      get(url)
        .then(res => {
          res.json().then(result => {
            let dataBuff = [];
            if (result.rows) {
              result.rows.map(item => {
                return dataBuff.push({
                  id: item.id,
                  title: item.consumerName
                });
              });
            }
            dispatch(actions.setSearchStatus(status.SUCCESS));
            dispatch(actions.setCurrentResultSearchSuggestion(dataBuff));
          });
        })
        .catch(err => {
          dispatch(actions.setSearchStatus(status.SUCCESS));
        });
    } else {
      dispatch(actions.setSearchStatus(status.ERROR));
      return dispatch(actions.setCurrentResultSearchSuggestion([]));
    }
  };
}
function addItemAsync(
  code,
  techCode,
  productId,
  name,
  showCodeFlag,
  showLotNoFlag
) {
  let url = `${BACKEND_URL}/items`;
  return dispatch => {
    dispatch(actions.setStatus(status.LOADING));
    post(
      url,
      JSON.stringify({
        code,
        techCode,
        productId,
        name,
        showCodeFlag,
        showLotNoFlag
      })
    )
      .then(res => {
        if (res.status === 200) {
          dispatch(actions.clearForm());
          dispatch(actions.setStatus(status.SUCCESS));
        } else {
          res.json().then(json => {
            dispatch(actions.setErrorCode(json));
            dispatch(actions.setStatus(status.ERROR));
          });
        }
      })
      .catch(err => {
        dispatch(actions.setStatus(status.ERROR));
      });
  };
}
function getItemByIdAsync(id) {
  id = id ? encodeURIComponent(id) : "";
  return dispatch => {
    dispatch(actions.setStatus(status.LOADING));
    let url = `${BACKEND_URL}/items/${id}`;
    get(url)
      .then(res => {
        if (res.ok) {
          res.json().then(json => {
            dispatch(actions.setItem(status.SUCCESS, json));
          });
        } else {
          dispatch(actions.setStatus(status.ERROR));
        }
      })
      .catch(err => {
        dispatch(actions.setStatus(status.ERROR));
      });
  };
}
function updateItemAsync(
  code,
  techCode,
  productId,
  name,
  showCodeFlag,
  showLotNoFlag,
  id
) {
  return dispatch => {
    dispatch(actions.setStatus(status.LOADING));
    let url = `${BACKEND_URL}/items/${id}`;
    put(
      url,
      JSON.stringify({
        code,
        techCode,
        productId,
        name,
        showCodeFlag,
        showLotNoFlag
      })
    )
      .then(res => {
        dispatch(
          res.ok
            ? actions.setStatus(status.SUCCESS)
            : actions.setStatus(status.ERROR)
        );
      })
      .catch(err => {
        dispatch(actions.setStatus(status.ERROR));
      });
  };
}

function setCurrentPageAsync(currentTablePage, keyword) {
  return dispatch => {
    dispatch(actions.setCurrentTablePage(currentTablePage));
    dispatch(actions.setCurrentSearchKeyword(keyword));
    store.dispatch(
      push({
        search: "?p=" + currentTablePage + "&keyword=" + keyword
      })
    );
  };
}

export const actions = {
  setSearchList: (status, total, list) => ({
    type: types.SET_SEARCH_LIST,
    payload: { status, total, list }
  }),
  setOpenModal: (modalIsOpen, status) => ({
    type: types.SET_MODAL_ISOPEN,
    payload: { modalIsOpen, status }
  }),
  setShowCodeFlag: showCodeFlag => ({
    type: types.SET_ITEM_CODE_FLAG,
    payload: { showCodeFlag: showCodeFlag }
  }),
  setShowLotNoFlag: showLotNoFlag => ({
    type: types.SET_LOT_NO_FLAG,
    payload: { showLotNoFlag: showLotNoFlag }
  }),
  setSelectedSuggestion: (productId, consumerName) => ({
    type: types.SET_SELECTED_SUGGESTION,
    payload: { productId, consumerName }
  }),
  setSearchStatus: searchStatus => ({
    type: types.SET_SEARCH_STATUS,
    payload: { searchStatus }
  }),
  setRowStatus: rowStatus => ({
    type: types.SET_ROW_STATUS,
    payload: { rowStatus }
  }),
  setCurrentResultSearchSuggestion: list => ({
    type: types.SET_CURRENT_RESULT_SEARCH_SUGGESTION,
    payload: { list }
  }),
  setCurrentTablePage: currentTablePage => ({
    type: types.SET_CURRENT_TABLE_PAGE,
    payload: { currentTablePage }
  }),
  setCurrentSearchKeyword: currentSearchKeyword => ({
    type: types.SET_CURRENT_KEYWORD,
    payload: { currentSearchKeyword }
  }),
  setErr: error => ({ type: types.SET_ERROR, payload: { error } }),
  clearForm: () => ({ type: types.CLEAR_ALL }),
  setStatus: status => ({ type: types.SET_STATUS, payload: { status } }),
  setCode: code => ({ type: types.SET_CODE, payload: { code } }),
  setTechCode: techCode => ({
    type: types.SET_TECH_CODE,
    payload: { techCode }
  }),
  setConsumerName: consumerName => ({
    type: types.SET_CONSUMER_NAME,
    payload: { consumerName }
  }),
  setItem: (status, item) => ({
    type: types.SET_ITEM,
    payload: { status, item }
  }),
  setName: name => ({ type: types.SET_NAME, payload: { name } }),
  setErrorCode: error => ({ type: types.SET_ERROR_CODE, payload: { error } }),
  addItemAsync,
  getItemByIdAsync,
  searchAsync,
  deleteOneAsync,
  searchSuggestionAsync,
  searchProductSuggestionAsync,
  updateItemAsync,
  setCurrentPageAsync
};
