import { BACKEND_URL } from "config/constants";
import { DEFAULT_PAGINATION_LIMIT } from "config/constants";
import { get, del } from "utils/fetch";
import { push } from "react-router-redux";
import store from "store";
import { types } from "./types";
import { postFormData } from "../../utils/fetch";

function getBrandList(resolve, rej) {
  const url = `${BACKEND_URL}/autofun/rest/car/brand`;
  return dispatch => {
    get(url).then(res => {
      res.json().then(json => {
        if (res.status === 200) {
          let dataBuffer = [{ value: "", title: "All" }];
          console.log(json.listBrand);
          json.listBrand.map(b => {
            return dataBuffer.push({ value: b.id, title: b.brandName });
          });
          dispatch(actions.setBrandList(dataBuffer));
          resolve();
        } else {
          rej();
        }
      });
    });
  };
}

function getGearList(resolve, rej) {
  const url = `${BACKEND_URL}/autofun/rest/car/gear`;
  return dispatch => {
    get(url).then(res => {
      res.json().then(json => {
        if (res.status === 200) {
          let dataBuffer = [{ value: "", title: "All" }];
          json.listGear.map(g => {
            return dataBuffer.push({ value: g.id, title: g.gearName });
          });
          dispatch(actions.setGearList(dataBuffer));
          resolve();
        } else {
          rej();
        }
      });
    });
  };
}

function getAllList() {
  return dispatch => {
    dispatch(
      actions.getBrandList(() => {
        dispatch(actions.getGearList(() => {}));
      })
    );
  };
}

function addCar(car) {
  return dispatch => {
    const url = `${BACKEND_URL}/autofun/admin/car/upload`;
    let body = new FormData();
    body.append("carName", car.name);
    body.append("description", car.description);
    body.append("manufactureYear", car.year);
    body.append("price", car.price);
    body.append("brandId", car.brand);
    body.append("status", car.status);
    body.append("carType", car.carType);
    body.append("numOfKm", car.numOfKm);
    body.append("exteriorColor", car.outerColor);
    body.append("interiorColor", car.innerColor);
    body.append("numOfDoor", car.numOfDoor);
    body.append("numOfSeat", car.numOfSeat);
    body.append("fuel", car.fuel);
    body.append("fuelSystem", car.fuelSystem);
    body.append("fuelConsumption", car.fuelComsumption);
    body.append("driving", car.actutor);
    body.append("gearId", car.gear);
    body.append("images", car.image);
    body.append("origin", car.origin);
    body.append("userId", 2);
    postFormData(url, body);
  };
}

export const actions = {
  setBrandList: brandList => ({
    type: types.SET_BRANDLIST,
    payload: { brandList }
  }),
  setGearList: gearList => ({
    type: types.SET_GEARLIST,
    payload: { gearList }
  }),
  setName: name => ({ type: types.SET_NAME, payload: { name } }),
  setBrand: brand => ({ type: types.SET_BRAND, payload: { brand } }),
  setYear: year => ({ type: types.SET_YEAR, payload: { year } }),
  setOrigin: origin => ({ type: types.SET_ORIGIN, payload: { origin } }),
  setStatus: status => ({ type: types.SET_STATUS, payload: { status } }),
  setCarType: carType => ({ type: types.SET_CARTYPE, payload: { carType } }),
  setNumOfKm: numOfKm => ({ type: types.SET_NUMOFKM, payload: { numOfKm } }),
  setPrice: price => ({ type: types.SET_PRICE, payload: { price } }),
  setOuterColor: outerColor => ({
    type: types.SET_OUTERCOLOR,
    payload: { outerColor }
  }),
  setInnerColor: innerColor => ({
    type: types.SET_INNERCOLOR,
    payload: { innerColor }
  }),
  setNumOfDoor: numOfDoor => ({
    type: types.SET_NUMOFDOOR,
    payload: { numOfDoor }
  }),
  setNumOfSeat: numOfSeat => ({
    type: types.SET_NUMOFSEAT,
    payload: { numOfSeat }
  }),
  setGear: gear => ({ type: types.SET_GEAR, payload: { gear } }),
  setActutor: actutor => ({ type: types.SET_ACTUTOR, payload: { actutor } }),
  setFuel: fuel => ({ type: types.SET_FUEL, payload: { fuel } }),
  setFuelSystem: fuelSystem => ({
    type: types.SET_FUEL_SYSTEM,
    payload: { fuelSystem }
  }),
  setFuelComsumption: fuelComsumption => ({
    type: types.SET_FUEL_COMSUMPTION,
    payload: { fuelComsumption }
  }),
  setDescription: description => ({
    type: types.SET_DESCRIPTION,
    payload: { description }
  }),
  setImage: image => ({ type: types.SET_IMAGE, payload: { image } }),
  getBrandList,
  getGearList,
  addCar,
  getAllList
};
