import { status } from "../../config/commonStatus";
// Actions
import { types } from "./types";

//Reducer
export const initialState = {
  brandList: [],
  gearList: [],
  name: "",
  brand: "",
  year: "",
  origin: "",
  status: "",
  carType: "",
  numOfKm: "",
  price: "",
  outerColor: "",
  innerColor: "",
  numOfDoor: "",
  numOfSeat: "",
  gear: "",
  actutor: "",
  fuel: "",
  fuelSystem: "",
  fuelComsumption: "",
  description: "",
  image: []
};

const Car = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_BRANDLIST:
      return { ...state, brandList: action.payload.brandList };
    case types.SET_GEARLIST:
      return { ...state, gearList: action.payload.gearList };
    //
    case types.SET_NAME:
      return { ...state, name: action.payload.name };
    case types.SET_BRAND:
      return { ...state, brand: action.payload.brand };
    case types.SET_YEAR:
      return { ...state, year: action.payload.year };
    case types.SET_ORIGIN:
      return { ...state, origin: action.payload.origin };
    case types.SET_STATUS:
      return { ...state, status: action.payload.status };
    case types.SET_CARTYPE:
      return { ...state, carType: action.payload.carType };
    case types.SET_NUMOFKM:
      return { ...state, numOfKm: action.payload.numOfKm };
    case types.SET_PRICE:
      return { ...state, price: action.payload.price };
    case types.SET_OUTERCOLOR:
      return { ...state, outerColor: action.payload.outerColor };
    case types.SET_INNERCOLOR:
      return { ...state, innerColor: action.payload.innerColor };
    case types.SET_NUMOFDOOR:
      return { ...state, numOfDoor: action.payload.numOfDoor };
    case types.SET_NUMOFSEAT:
      return { ...state, numOfSeat: action.payload.numOfSeat };
    case types.SET_GEAR:
      return { ...state, gear: action.payload.gear };
    case types.SET_ACTUTOR:
      return { ...state, actutor: action.payload.actutor };
    case types.SET_FUEL:
      return { ...state, fuel: action.payload.fuel };
    case types.SET_FUEL_SYSTEM:
      return { ...state, fuelSystem: action.payload.fuelSystem };
    case types.SET_FUEL_COMSUMPTION:
      return { ...state, fuelComsumption: action.payload.fuelComsumption };
    case types.SET_DESCRIPTION:
      return { ...state, description: action.payload.description };
    case types.SET_IMAGE:
      return { ...state, image: action.payload.image };

    case types.SET_CAR:
      let item = action.payload.item;
      return {
        ...state,
        name: item.name,
        brand: item.brand,
        year: item.year,
        origin: item.origin,
        status: item.status,
        carType: item.carType,
        numOfKm: item.numOfKm,
        price: item.price,
        outerColor: item.outerColor,
        innerColor: item.innerColor,
        numOfDoor: item.numOfDoor,
        numOfSeat: item.numOfSeat,
        gear: item.gear,
        actutor: item.actutor,
        fuel: item.fuel,
        fuelSystem: item.fuelSystem,
        fuelComsumption: item.fuelComsumption,
        description: item.description,
        image: item.image
      };
    default:
      return state;
  }
};

export default Car;
