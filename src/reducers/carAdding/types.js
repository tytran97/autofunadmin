export const types = {
  SET_BRANDLIST: "SETCOMBOBOX/SET_BRANDLIST",
  SET_GEARLIST: "SETCOMBOBOX/SET_GEALIST",
  /*
   *  ITEM CREATE
   */

  //Basic Information
  SET_NAME: "ITEM_CREATE/SET_NAME",
  SET_BRAND: "ITEM_CREATE/SET_BRAND",
  SET_YEAR: "ITEM_CREATE/SET_YEAR",
  SET_ORIGIN: "ITEM_CREATE/SET_ORIGIN",
  SET_STATUS: "ITEM_CREATE/SET_STATUS",
  SET_CARTYPE: "ITEM_CREATE/SET_CARTYPE",
  SET_NUMOFKM: "ITEM_CREATE/SET_NUMOFKM",
  SET_PRICE: "ITEM_CREATE/SET_PRICE",
  SET_OUTERCOLOR: "ITEM_CREATE/SET_OUTERCOLOR",
  SET_INNERCOLOR: "ITEM_CREATE/SET_INNERCOLOR",
  SET_NUMOFDOOR: "ITEM_CREATE/SET_NUMOFDOOR",
  SET_NUMOFSEAT: "ITEM_CREATE/SET_NUMOFSEAT",
  //GearBox
  SET_GEAR: "ITEM_CREATE/SET_GEAR",
  SET_ACTUTOR: "ITEM_CREATE/SET_ACTUTOR",
  //Fuel
  SET_FUEL: "ITEM_CREATE/SET_FUEL",
  SET_FUEL_SYSTEM: "ITEM_CREATE/SET_FUEL_SYSTEM",
  SET_FUEL_COMSUMPTION: "ITEM_CREATE/SET_FUEL_COMSUMPTION",
  //More Information
  SET_DESCRIPTION: "ITEM_CREATE/SET_DESCRIPTION",
  SET_IMAGE: "ITEM_CREATE/SET_IMAGE",
  //
  SET_CAR: "ITEM_CREATE/SET_CAR"
};
