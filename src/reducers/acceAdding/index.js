import { status } from "../../config/commonStatus";
// Actions
import { types } from "./types";

//Reducer
export const initialState = {
  brandList: [],
  typeList: [],
  name: "",
  brand: "",
  acceType: "",
  price: "",
  quantity: "",
  description: "",
  image: []
};

const Acce = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_BRANDLIST:
      return { ...state, brandList: action.payload.brandList };
    case types.SET_TYPELIST:
      return { ...state, typeList: action.payload.typeList };
    //
    case types.SET_NAME:
      return { ...state, name: action.payload.name };
    case types.SET_BRAND:
      return { ...state, brand: action.payload.brand };
    case types.SET_ACCETYPE:
      return { ...state, acceType: action.payload.acceType };
    case types.SET_PRICE:
      return { ...state, price: action.payload.price };
    case types.SET_QUANTITY:
      return { ...state, quantity: action.payload.quantity };
    case types.SET_DESCRIPTION:
      return { ...state, description: action.payload.description };
    case types.SET_IMAGE:
      return { ...state, image: action.payload.image };

    case types.SET_ACCE:
      let item = action.payload.item;
      return {
        ...state,
        name: item.name,
        brand: item.brand,
        acceType: item.acceType,
        price: item.price,
        quantity: item.quantity,
        description: item.description,
        image: item.image
      };
    default:
      return state;
  }
};

export default Acce;
