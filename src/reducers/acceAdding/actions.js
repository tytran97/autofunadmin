import { BACKEND_URL } from "config/constants";
import { DEFAULT_PAGINATION_LIMIT } from "config/constants";
import { get, del } from "utils/fetch";
import { push } from "react-router-redux";
import store from "store";
import { types } from "./types";
import { postFormData } from "../../utils/fetch";

function getBrandList(resolve, rej) {
  const url = `${BACKEND_URL}/autofun/rest/car/brand`;
  return dispatch => {
    get(url).then(res => {
      res.json().then(json => {
        if (res.status === 200) {
          let dataBuffer = [{ value: "", title: "All" }];
          console.log(json.listBrand);
          json.listBrand.map(b => {
            return dataBuffer.push({ value: b.id, title: b.brandName });
          });
          dispatch(actions.setBrandList(dataBuffer));
          resolve();
        } else {
          () => rej();
        }
      });
    });
  };
}

function getTypeList(resolve, rej) {
  const url = `${BACKEND_URL}/autofun/rest/car/type`;
  return dispatch => {
    get(url).then(res => {
      res.json().then(json => {
        if (res.status === 200) {
          let dataBuffer = [{ value: "", title: "All" }];
          json.listGear.map(g => {
            return dataBuffer.push({ value: g.id, title: g.gearName });
          });
          dispatch(actions.setGearList(dataBuffer));
          resolve();
        } else {
          () => rej();
        }
      });
    });
  };
}

function getAllList() {
  return dispatch => {
    dispatch(
      actions.getBrandList(() => {
        dispatch(actions.getTypeList(() => {}));
      })
    );
  };
}

function addAcce(acce) {
  return dispatch => {
    const url = `${BACKEND_URL}/autofun/admin/accessories/upload`;
    let body = new FormData();
    body.append("accessoriesName", acce.name);
    body.append("description", acce.description);
    body.append("quantity", acce.quantity);
    body.append("price", acce.price);
    body.append("brandId", acce.brand);
    body.append("typeId", acce.acceType);
    body.append("images", acce.image);
    body.append("userId", 2);
    postFormData(url, body);
  };
}

export const actions = {
  setBrandList: brandList => ({
    type: types.SET_BRANDLIST,
    payload: { brandList }
  }),
  setTypeList: typeList => ({
    type: types.SET_TYPELIST,
    payload: { typeList }
  }),
  setName: name => ({ type: types.SET_NAME, payload: { name } }),
  setBrand: brand => ({ type: types.SET_BRAND, payload: { brand } }),
  setAcceType: acceType => ({
    type: types.SET_ACCETYPE,
    payload: { acceType }
  }),
  setNumOfKm: numOfKm => ({ type: types.SET_NUMOFKM, payload: { numOfKm } }),
  setPrice: price => ({ type: types.SET_PRICE, payload: { price } }),
  setQuantity: quantity => ({
    type: types.SET_QUANTITY,
    payload: { quantity }
  }),
  setDescription: description => ({
    type: types.SET_DESCRIPTION,
    payload: { description }
  }),
  setImage: image => ({ type: types.SET_IMAGE, payload: { image } }),
  getBrandList,
  getTypeList,
  addAcce,
  getAllList
};
