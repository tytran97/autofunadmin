import fetch from "node-fetch";
import Cookies from "js-cookie";
import swal from "sweetalert";

import { BACKEND_URL } from "config/constants";

export const types = {
  USER_LOGIN: "MAIN/USER_LOGIN",
  SET_STATUS: "MAIN/SET_STATUS"
};

export const initialState = {
  status: "unauthorized"
};

function getLoginAsync(username, password) {
  return dispatch => {
    dispatch(actions.setStatus("loading"));
    let url = `${BACKEND_URL}/autofun/rest/user/login`;

    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ username, password })
    };

    fetch(url, requestOptions)
      .then(res => {
        res
          .json()
          .then(json => {
            if (json.status === 401) {
              swal({
                title: "Đăng nhập thất bại!",
                icon: "error",
                button: "Đồng ý"
              });
              dispatch(actions.setStatus("error"));
            } else if (json.role.id !== 2) {
              swal({
                title: "Bạn không có quyền!",
                icon: "error",
                button: "Đồng ý"
              });
              dispatch(actions.setStatus("error"));
            } else {
              Cookies.set("accessToken", json.token);
              dispatch(actions.setStatus("success"));
            }
          })
          .catch(err => {
            dispatch(actions.setStatus("error"));
          });
      })
      .catch(err => {
        dispatch(actions.setStatus("error"));
      });
  };
}

const authInfo = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_STATUS:
      return { ...state, status: action.payload.status };
    default:
      return { ...state };
  }
};

export default authInfo;

export const actions = {
  setStatus: (status, content) => ({
    type: types.SET_STATUS,
    payload: { status, content }
  }),
  getLoginAsync
};
