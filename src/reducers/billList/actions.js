import { BACKEND_URL } from "config/constants";
import { DEFAULT_PAGINATION_LIMIT } from "config/constants";
import { get, del } from "utils/fetch";
import { push } from "react-router-redux";
import store from "store";
import { types } from "./types";
import { status } from "../../config/commonStatus";

function searchAsync(
  keyword = "",
  offset = 0,
  limit = DEFAULT_PAGINATION_LIMIT
) {
  return dispatch => {
    if (!keyword) keyword = "";
    else keyword = encodeURIComponent(keyword);
    dispatch(actions.setStatus(status.LOADING));
    const url = `${BACKEND_URL}/autofun/admin/bill/list?offset=${offset}&keyword=${keyword}&limit=${limit}`;
    get(url)
      .then(res => {
        res.json().then(result => {
          if (res.status === 200) {
            dispatch(
              actions.setSearchList(
                status.SUCCESS,
                result.totalPages,
                result.bills
              )
            );
            localStorage.setItem("item-count", result.bills.length);
          } else {
            dispatch(actions.setSearchList(status.ERROR));
            localStorage.removeItem("item-count");
          }
        });
      })
      .catch(err => {
        dispatch(actions.setSearchList(status.ERROR));
        localStorage.removeItem("item-count");
      });
  };
}

function searchSuggestionAsync(searchValue) {
  return dispatch => {
    if (searchValue.length > 0) {
      dispatch(actions.setSearchStatus(status.LOADING));
      const url = `${BACKEND_URL}/autofun/admin/bill/list?limit=10&offset=0&keyword=${searchValue}`;
      get(url)
        .then(res => {
          res.json().then(result => {
            dispatch(actions.setSearchStatus(status.SUCCESS));
            let dataBuff = [];
            result.listCarAccessories.map(item => {
              return dataBuff.push({
                id: item.id,
                title: item.accessoriesName
              });
            });
            dispatch(actions.setCurrentResultSearchSuggestion(dataBuff));
          });
        })
        .catch(err => {
          dispatch(actions.setSearchStatus(status.ERROR));
        });
    } else {
      dispatch(actions.setSearchStatus(status.ERROR));
      return dispatch(actions.setCurrentResultSearchSuggestion([]));
    }
  };
}

function setCurrentPageAsync(currentTablePage, keyword) {
  return dispatch => {
    dispatch(actions.setCurrentTablePage(currentTablePage));
    dispatch(actions.setCurrentSearchKeyword(keyword));
    store.dispatch(
      push({
        search: "?p=" + currentTablePage + "&keyword=" + keyword
      })
    );
  };
}

export const actions = {
  setSearchList: (status, totalPages, list) => ({
    type: types.SET_SEARCH_LIST,
    payload: { status, totalPages, list }
  }),
  setOpenModal: (modalIsOpen, status) => ({
    type: types.SET_MODAL_ISOPEN,
    payload: { modalIsOpen, status }
  }),
  setShowCodeFlag: showCodeFlag => ({
    type: types.SET_ITEM_CODE_FLAG,
    payload: { showCodeFlag: showCodeFlag }
  }),
  setShowLotNoFlag: showLotNoFlag => ({
    type: types.SET_LOT_NO_FLAG,
    payload: { showLotNoFlag: showLotNoFlag }
  }),
  setSelectedSuggestion: (productId, consumerName) => ({
    type: types.SET_SELECTED_SUGGESTION,
    payload: { productId, consumerName }
  }),
  setSearchStatus: searchStatus => ({
    type: types.SET_SEARCH_STATUS,
    payload: { searchStatus }
  }),
  setRowStatus: rowStatus => ({
    type: types.SET_ROW_STATUS,
    payload: { rowStatus }
  }),
  setCurrentResultSearchSuggestion: list => ({
    type: types.SET_CURRENT_RESULT_SEARCH_SUGGESTION,
    payload: { list }
  }),
  setCurrentTablePage: currentTablePage => ({
    type: types.SET_CURRENT_TABLE_PAGE,
    payload: { currentTablePage }
  }),
  setCurrentSearchKeyword: currentSearchKeyword => ({
    type: types.SET_CURRENT_KEYWORD,
    payload: { currentSearchKeyword }
  }),
  setErr: error => ({ type: types.SET_ERROR, payload: { error } }),
  clearForm: () => ({ type: types.CLEAR_ALL }),
  setStatus: status => ({ type: types.SET_STATUS, payload: { status } }),
  setCode: code => ({ type: types.SET_CODE, payload: { code } }),
  setTechCode: techCode => ({
    type: types.SET_TECH_CODE,
    payload: { techCode }
  }),
  setConsumerName: consumerName => ({
    type: types.SET_CONSUMER_NAME,
    payload: { consumerName }
  }),
  setItem: (status, item) => ({
    type: types.SET_ITEM,
    payload: { status, item }
  }),
  setName: name => ({ type: types.SET_NAME, payload: { name } }),
  setErrorCode: error => ({ type: types.SET_ERROR_CODE, payload: { error } }),
  searchAsync,
  searchSuggestionAsync,
  setCurrentPageAsync
};
