import { status } from "../../config/commonStatus";
// Actions
import { types } from "./types";

//Reducer
export const initialState = {
  modalIsOpen: false,
  searchStatus: status.SUCCESS,
  status: null,
  totalPages: 0,
  list: [],
  showLotNoFlag: true,
  showCodeFlag: true,
  currentTablePage: 1,
  currentSearchKeyword: "",
  productId: -1,
  consumerName: "",
  currentResultSearchSuggestion: [],
  error: {
    code: null,
    codeMsg: "",
    techCode: "",
    consumerName: "",
    name: "",
    ERR: null
  },
  code: "",
  techCode: 1,
  name: "",
  rowStatus: null
};

const UserList = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SEARCH_LIST:
      return {
        ...state,
        status: action.payload.status,
        totalPages: action.payload.totalPages,
        list: action.payload.list || []
      };
    case types.SET_MODAL_ISOPEN:
      return {
        ...state,
        modalIsOpen: action.payload.modalIsOpen,
        status: action.payload.status
      };
    case types.SET_LOT_NO_FLAG:
      return { ...state, showLotNoFlag: action.payload.showLotNoFlag };
    case types.SET_ITEM_CODE_FLAG:
      return { ...state, showCodeFlag: action.payload.showCodeFlag };
    case types.SET_SELECTED_SUGGESTION:
      return {
        ...state,
        productId: action.payload.productId || -1,
        consumerName: action.payload.consumerName || ""
      };
    case types.SET_CURRENT_RESULT_SEARCH_SUGGESTION:
      let list = action.payload.list;

      if (list) {
        return {
          ...state,
          currentResultSearchSuggestion: list,
          error: { ...state.error, consumerName: "" }
        };
      } else {
        return { ...state, currentResultSearchSuggestion: [] };
      }

    case types.CLEAR_CURRENT_RESULT_SEARCH_SUGGESTION:
      return { ...state, currentResultSearchSuggestion: [] };
    case types.SET_CURRENT_TABLE_PAGE:
      return { ...state, currentTablePage: action.payload.currentTablePage };
    case types.SET_SEARCH_STATUS:
      return { ...state, searchStatus: action.payload.searchStatus };
    case types.SET_ROW_STATUS:
      return { ...state, rowStatus: action.payload.rowStatus };
    case types.SET_ERROR:
      return { ...state, error: action.payload.error || state.error };
    case types.SET_ERROR_CODE:
      return { ...state, error: { code: action.payload.error.error.code } };
    case types.SET_STATUS:
      return { ...state, status: action.payload.status };
    case types.SET_CURRENT_KEYWORD:
      return {
        ...state,
        currentSearchKeyword: action.payload.currentSearchKeyword
      };
    case types.SET_CODE:
      return { ...state, code: action.payload.code };
    case types.SET_TECH_CODE:
      return { ...state, techCode: action.payload.techCode };
    case types.SET_CONSUMER_NAME:
      return { ...state, consumerName: action.payload.consumerName };
    case types.SET_NAME:
      return { ...state, name: action.payload.name };
    case types.CLEAR_ALL:
      return {
        ...state,
        error: {
          codeMsg: "",
          techCode: "",
          consumerName: "",
          name: ""
        },
        status: null,
        code: "",
        techCode: 1,
        consumerName: "",
        currentResultSearchSuggestion: [],
        name: "",
        productId: -1
      };
    case types.SET_ITEM:
      let item = action.payload.item;
      if (item) {
        return {
          ...state,
          status: null,
          code: item.code,
          techCode: item.techCode,
          consumerName: item.Product ? item.Product.consumerName : "",
          showCodeFlag: item.showCodeFlag,
          showLotNoFlag: item.showLotNoFlag,
          name: item.name,
          productId: item.Product ? item.Product.id : ""
        };
      } else {
        return { ...state, status: status.LOADING };
      }
    default:
      return state;
  }
};

export default UserList;
