import React from "react";
import ReactPaginate from "react-paginate";
import { actions as carActions } from "reducers/carList/actions";
import { actions as userActions } from "reducers/userList/actions";
import { actions as acceActions } from "reducers/acceList/actions";
import { actions as billActions } from "reducers/billList/actions";
import { DEFAULT_PAGINATION_LIMIT } from "config/constants";
import "./DataTable.css";
import { connect } from "react-redux";
import Row from "./Row";
import { getParam } from "utils/urlParam";
import {
  CarHeaders,
  UserHeaders,
  AcceHeaders,
  BillHeaders
} from "config/commonHeaders";
import { bindActionCreators } from "redux";
class Rows extends React.Component {
  render() {
    let {
      data,
      currentPage,
      delRowHandle,
      doubleClickRowHandle,
      onClosePopupSuccess,
      onClosePopup,
      rowStatus
    } = this.props;
    return (
      <tbody>
        {data.map((rowdata, index) => (
          <Row
            rowStatus={rowStatus}
            type={this.props.type}
            key={data.id || index}
            dataRow={rowdata}
            currentPage={currentPage}
            delRowHandle={delRowHandle}
            doubleClickRowHandle={doubleClickRowHandle}
            onClosePopup={onClosePopup}
            onClosePopupSuccess={onClosePopupSuccess}
          />
        ))}
      </tbody>
    );
  }
}

class Header extends React.Component {
  render() {
    let { headers } = this.props;
    let renderHeader = [];
    headers.map((header, index) => {
      return renderHeader.push(<th key={index}>{header && header.value}</th>);
    });
    return (
      <thead>
        <tr>{renderHeader}</tr>
      </thead>
    );
  }
}

class DataTable extends React.Component {
  constructor(props) {
    super(props);
    this.initNode = this.initNode.bind(this);
    this.initNode();
    this.state = {
      rowDeletedIndex: 0
    };
  }
  initNode() {
    let currentPage = this.props.item.currentTablePage;
    let currentSearchKeyword = this.props.item.currentSearchKeyword;
    let count =
      localStorage.getItem("item-count") / DEFAULT_PAGINATION_LIMIT + 1;

    let p = getParam("p");
    let k = getParam("keyword");
    let page;
    let keyword = "";
    if (p) {
      if (p > count || p <= 0) {
        page = 1;
      } else {
        page = p;
      }
      keyword = k ? k : "";
    } else {
      page = currentPage;
      keyword = currentSearchKeyword;
    }
    this.props.actions.setCurrentPageAsync(
      page,
      keyword === "null" ? "" : keyword
    );
  }
  componentWillMount() {
    let currentSearchKeyword = this.props.item.currentSearchKeyword;
    let currentPage = this.props.item.currentTablePage;
    //let currentPage = 2;
    let p = getParam("p");
    let keyword = getParam("keyword");
    if (this.props.item.list.length > 0) {
      this.props.actions.setStatus("SUCCESS");
    } else {
      this.props.actions.setCurrentPageAsync(
        p,
        keyword === "null" || keyword === "0" ? "" : keyword
      );

      if (p !== "null") {
        this.props.actions.searchAsync(
          keyword === "null" || keyword === "0" ? "" : keyword,
          p - 1
        );
      } else {
        this.props.actions.searchAsync(currentSearchKeyword, currentPage - 1);
      }
    }
  }
  delRowHandle(id, index, numberOfRow) {
    let { item } = this.props;
    this.props.actions
      .deleteItem(id)
      .then(() => {
        console.log("done");
        if (
          index === (item.currentTablePage - 1) * 10 + 1 &&
          numberOfRow === 1
        ) {
          this.props.actions.setCurrentPageAsync(
            item.currentTablePage - 1 === 0 ? 1 : item.currentTablePage - 1
          );
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    let { item } = this.props;
    let { searchAsync, setCurrentPageAsync } = this.props.actions;
    let data = this.props.data;
    let headers = this.props.headers;
    let handlePageChange = page => {
      let offset = page - 1;
      setCurrentPageAsync(page, item.currentSearchKeyword);
      searchAsync(item.currentSearchKeyword, offset);
    };
    let delRowHandle = (id, index, numberOfRow) => {
      this.delRowHandle(id, index, numberOfRow);
    };
    let { totalPages, currentTablePage } = item;
    let currentPage = currentTablePage;
    let { doubleClickRowHandle } = this.props;
    const pageCount = totalPages;
    let onClosePopup = () => {
      this.props.actions.setRowStatus(null);
    };
    let onClosePopupSuccess = () => {
      this.props.actions.setRowStatus(null);
      this.props.actions.searchAsync(
        item.currentSearchKeyword,
        item.currentTablePage - 1
      );
    };

    return (
      <div className="row data-list">
        <table className="table">
          <Header headers={headers} />
          <Rows
            rowStatus={this.props.item.rowStatus}
            type={this.props.type}
            data={data}
            delRowHandle={delRowHandle}
            currentPage={currentPage}
            doubleClickRowHandle={doubleClickRowHandle}
            onClosePopup={onClosePopup}
            onClosePopupSuccess={onClosePopupSuccess}
          />
        </table>
        {pageCount === 0 && (
          <div className="empty-body">Không tìm thấy sản phẩm nào.</div>
        )}
        {pageCount > 0 && (
          <ReactPaginate
            previousLabel={"<"}
            nextLabel={">"}
            breakLabel={<a href="">...</a>}
            breakClassName={"break-me"}
            pageCount={pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            forcePage={currentPage - 1}
            onPageChange={page => {
              handlePageChange(page.selected + 1);
            }}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  let type = ownProps.type;
  let item;
  let data;
  switch (type) {
    case "carList":
      item = state.item;
      data = [];
      if (item.list) {
        item.list.map(item => {
          let dataRow = [];
          dataRow.push(
            { id: item.id, isId: true },
            { value: item.id },
            { value: item.carName },
            { value: item.brand ? item.brand.brandName : "" },
            { value: item.manufactureYear },
            { value: item.price }
          );
          return data.push(dataRow);
        });
      }
      return {
        data: data,
        item: state.carList,
        headers: CarHeaders
      };
    case "userList":
      item = state.item;
      data = [];
      if (item.list) {
        item.list.map(item => {
          let dataRow = [];
          dataRow.push(
            { id: item.id, isId: true },
            { value: item.id },
            { value: item.userName },
            { value: item.lastName + " " + item.firstName },
            { value: item.email },
            { value: item.phoneNumber }
          );
          return data.push(dataRow);
        });
      }
      return {
        data: data,
        item: state.userList,
        headers: UserHeaders
      };
    case "acceList":
      item = state.item;
      data = [];
      if (item.list) {
        item.list.map(item => {
          let dataRow = [];
          dataRow.push(
            { id: item.id, isId: true },
            { value: item.id },
            { value: item.accessoriesName },
            { value: item.price },
            { value: item.quantity }
          );
          return data.push(dataRow);
        });
      }
      return {
        data: data,
        item: state.acceList,
        headers: AcceHeaders
      };
    case "billList":
      item = state.item;
      data = [];
      if (item.list) {
        item.list.map(item => {
          let dataRow = [];
          dataRow.push(
            { id: item.id, isId: true },
            { value: item.id },
            { value: item.status },
            {
              value: item.user
                ? `${item.user.firstName} ${item.user.lastName}`
                : ""
            },
            { id: item.id, isId: true }
          );
          return data.push(dataRow);
        });
      }
      return {
        data: data,
        item: state.billList,
        headers: BillHeaders
      };
    default:
      return {
        item: []
      };
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  let type = ownProps.type;
  let actions;
  switch (type) {
    case "carList":
      actions = carActions;
      break;
    case "userList":
      actions = userActions;
      break;
    case "acceList":
      actions = acceActions;
      break;
    case "billList":
      actions = billActions;
      break;
    default:
      break;
  }
  return {
    actions: bindActionCreators(actions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DataTable);
