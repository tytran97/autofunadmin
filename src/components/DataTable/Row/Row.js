import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import CustomPopup from "components/CustomPopup";
import { status as Status } from "config/commonStatus";

import "./Row.css";

class Row extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false
    };
    this.openModal = this.openModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.deleteRow = this.deleteRow.bind(this);
    this.restoreRow = this.restoreRow.bind(this);
    this.popupOkBtnFunction = this.popupOkBtnFunction.bind(this);
  }
  popupOkBtnFunction(e) {
    e.stopPropagation();
    const rowStatus = this.props.rowStatus;
    switch (rowStatus) {
      case Status.SUCCESS:
        this.props.onClosePopupSuccess();
        this.setState({ modalIsOpen: false });
        break;
      default:
        this.deleteRow();
        break;
    }
  }
  openModal(e) {
    e.stopPropagation();
    this.setState({ modalIsOpen: true });
  }

  onCloseModal() {
    this.props.onClosePopup();
    this.setState({ modalIsOpen: false });
  }
  deleteRow() {
    this.props.delRowHandle(
      this.props.dataRow[0].id,
      this.props.index,
      this.props.numberOfRow
    );
  }
  restoreRow() {
    this.props.restoreRowHandle(
      this.props.dataRow[0].id,
      this.props.index,
      this.props.numberOfRow
    );
  }
  handldeOnClickItem(id) {
    this.props.onClickRowHandle(id);
  }
  render() {
    let {
      dataRow,
      rowStatus,
      disabled,
      urlDetail,
      errorDelete,
      isQRScreen
    } = this.props;
    const { modalIsOpen } = this.state;
    let isShowOk, isShowCancel, cancelBtnLabel, classCancel;
    let isLoading = this.props.rowStatus === Status.LOADING;
    let isError = this.props.rowStatus === Status.ERROR;
    let message = "Bạn có muốn xóa";
    switch (rowStatus) {
      case "SUCCESS":
        isShowCancel = false;
        message = "Xóa thành công";
        break;
      case "ERROR":
        isShowOk = false;
        classCancel = "custom-delete_popup";
        cancelBtnLabel = "OK";
        message = "Có lỗi xảy ra";
        break;
      case "LOADING":
        isShowOk = false;
        isShowCancel = false;
        classCancel = "custom-delete_popup";
        message = "Đang xóa";
        break;
      default:
        break;
    }
    let button;
    if (
      dataRow[2].value == null ||
      dataRow[2].value === "done" ||
      dataRow[2].value === "cancel"
    ) {
      button = <div />;
    } else if (dataRow[2].value === "new") {
      button = (
        <td>
          <button className="acceptBtn">Duyệt đơn</button>
          <button className="cancelBtn">Hủy đơn</button>
        </td>
      );
    } else if (dataRow[2].value === "in progressing") {
      button = (
        <td>
          <button className="doneBtn">Hoàn thành</button>
          <button className="cancelBtn">Hủy đơn</button>
        </td>
      );
    }
    return (
      <React.Fragment>
        <tr style={disabled ? { backgroundColor: "#dddddd" } : {}}>
          {this.props.type === "carList" ||
          this.props.type === "acceList" ||
          this.props.type === "billList" ? (
            <td className="info-list-data--right">
              {disabled ? (
                <i className="far fa fa-undo" onClick={this.openModal} />
              ) : (
                <i className="far fa-trash-alt" onClick={this.openModal} />
              )}
              <Link to={urlDetail + "?id=" + encodeURIComponent(dataRow[0].id)}>
                <i className="icon-detail far fa-edit" />
              </Link>
            </td>
          ) : (
            <td>
              <Link to={urlDetail + "?id=" + encodeURIComponent(dataRow[0].id)}>
                <i className="icon-detail far fa-edit" />
              </Link>
            </td>
          )}

          {dataRow.map((item, index) => {
            if (item.isId == null) {
              return item.isIcon ? (
                <td key={index} className="info-list-data__type">
                  <img
                    alt={index}
                    width={(item.width || "20") + "px"}
                    src={item.value}
                  />
                </td>
              ) : (
                <td title={item.value} key={index}>
                  {item.value}
                </td>
              );
            } else {
              return null;
            }
          })}
          {button}
        </tr>
        <CustomPopup
          isOpen={modalIsOpen}
          message={message}
          errorMessage={errorDelete ? errorDelete : null}
          status={rowStatus}
          classCancel={classCancel}
          cancelBtnLabel={cancelBtnLabel}
          showCancelBtn={isShowCancel}
          showOkBtn={isShowOk}
          okBtnFunction={this.popupOkBtnFunction}
          onClose={this.onCloseModal}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Row);
