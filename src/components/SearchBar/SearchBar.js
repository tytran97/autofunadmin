import React from "react";
import { AsyncTypeahead } from "react-bootstrap-typeahead";
import { connect } from "react-redux";
import "./SearchBar.css";
import { bindActionCreators } from "redux";
import { actions as carActions } from "reducers/carList/actions";
import { actions as userActions } from "reducers/userList/actions";
import { actions as acceActions } from "reducers/acceList/actions";
import { actions as billActions } from "reducers/billList/actions";
import { getParam } from "utils/urlParam";
class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
    this.onInputChangeHandle = this.onInputChangeHandle.bind(this);
    this.onSelectSuggestion = this.onSelectSuggestion.bind(this);
    this.renderMenuDescriptionHandler = this.renderMenuDescriptionHandler.bind(
      this
    );
    this.clearSearchValue = this.clearSearchValue.bind(this);
  }
  renderMenuDescriptionHandler(option, props, index) {
    let { showDescription } = this.props;
    let title = option.title;
    let lowercaseTitle = title.toLowerCase();
    let lowercaseText = props.text.toLowerCase();
    let highlightStart = lowercaseTitle.indexOf(lowercaseText);
    let beforeHighlight = title.slice(0, highlightStart);
    let highlight = title.slice(
      highlightStart,
      highlightStart + lowercaseText.length
    );
    let afterHighlight = title.slice(highlightStart + lowercaseText.length);
    let template = (
      <React.Fragment>
        <div>
          <span>{beforeHighlight}</span>
          <span className="rbt-highlight-text">{highlight}</span>
          <span>{afterHighlight}</span>
          <br />
        </div>
        <div className="description">{option.value}</div>
      </React.Fragment>
    );
    let noneDescriptionTemplate = (
      <React.Fragment>
        <span>{beforeHighlight}</span>
        <span className="rbt-highlight-text">{highlight}</span>
        <span>{afterHighlight}</span>
      </React.Fragment>
    );
    return showDescription ? template : noneDescriptionTemplate;
  }
  clearSearchValue() {
    this._typeahead.getInstance().clear();
  }
  onSelectSuggestion(v) {
    if (v.length > 0) {
      this.setState({ value: v[0].title });
      if (this.props.onSelectSuggestion) {
        this.props.onSelectSuggestion(v[0].id, v[0].title);
      }
    }
  }
  onInputChangeHandle(e) {
    this.setState({ value: e });
    if (this.props.onSelectSuggestion) {
      this.props.onSelectSuggestion();
    }
  }

  render() {
    let { item } = this.props;
    console.log("==> item: ", item);
    let {
      searchSuggestionAsync,
      searchAsync,
      setCurrentPageAsync
    } = this.props.actions;
    let {
      currentSearchKeyword,
      label,
      labelClassName,
      inputClassName,
      disabled,
      inputClassNameWithLabel,
      inputClassNameWithoutLabel
    } = this.props;
    let btnSearchOnclick = keyword => {
      this.props.actions.setCurrentSearchKeyword(keyword);
      searchAsync(keyword);
      setCurrentPageAsync(1, keyword);
    };
    let { status } = item.searchStatus;
    let resultSearchSuggestion = item.currentResultSearchSuggestion;
    let showSearchBtn = true;
    let handleSearchSuggestion = searchSuggestionAsync;
    let placeholder;
    if (this.props.type === "carList") {
      placeholder = "Nhập loại xe cần tìm";
    } else if (this.props.type === "acceList") {
      placeholder = "Nhập loại sản phẩm cần tìm";
    }
    let keyword = getParam("keyword");
    let checkKeyword;
    if (!currentSearchKeyword) {
      checkKeyword = keyword === null || keyword === "0" || keyword === 0;
    } else checkKeyword = true;
    return (
      <React.Fragment>
        <div className="input-group search-component">
          {label ? (
            <div className={labelClassName + " label"}>{label}</div>
          ) : null}
          <div
            className={
              (inputClassName || label
                ? inputClassNameWithLabel
                : inputClassNameWithoutLabel) + " input-container"
            }
          >
            <i className="fas fa-search icon-search" />
            <AsyncTypeahead
              renderMenuItemChildren={this.renderMenuDescriptionHandler}
              disabled={disabled}
              defaultInputValue={checkKeyword ? currentSearchKeyword : keyword}
              useCache={false}
              allowNew={false}
              isLoading={status === "LOADING"}
              multiple={false}
              options={resultSearchSuggestion}
              labelKey="title"
              minLength={1}
              onInputChange={this.onInputChangeHandle}
              onSearch={e => {
                handleSearchSuggestion(e);
              }}
              placeholder={placeholder}
              onChange={this.onSelectSuggestion}
              ref={ref => (this._typeahead = ref)}
            />
          </div>

          {showSearchBtn && (
            <div className="col-md-2 lhtd-btn-search">
              <button
                className="lhtd-button"
                onClick={() => btnSearchOnclick(this.state.value)}
              >
                Tìm kiếm
              </button>
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

SearchBar.defaultProps = {
  labelClassName: "input-group-addon col-5 col-md-5 col-sm-5 col-lg-5",
  inputClassNameWithLabel: "col-6 col-md-6 col-sm-6 col-lg-6",
  inputClassNameWithoutLabel: "col-9 col-md-9 col-sm-9 col-lg-9"
};
function mapStateToProps(state, ownProps) {
  let type = ownProps.type;
  switch (type) {
    case "carList":
      return {
        item: state.carList
      };
    case "userList":
      return {
        item: state.userList
      };
    case "acceList":
      return {
        item: state.acceList
      };
    case "billList":
      return {
        item: state.billList
      };
    default:
      return {
        item: []
      };
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  let type = ownProps.type;
  let actions;

  switch (type) {
    case "carList":
      actions = carActions;
      break;
    case "userList":
      actions = userActions;
      break;
    case "acceList":
      actions = acceActions;
      break;
    case "billList":
      actions = billActions;
      break;
    default:
      break;
  }
  return {
    actions: bindActionCreators(actions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
