import React from "react";

import "./ComboBox.css";

class ComboBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modified: false
    };
  }

  onChangeHandler(e) {
    this.props.onChange(e);
  }

  render() {
    let {
      label,
      value,
      name,
      id,
      data,
      errorText,
      inputClassName,
      labelClassName
    } = this.props;
    if (this.state.modified) {
      errorText = "";
    }
    let inputClass = inputClassName;
    inputClass += errorText ? " errorInput" : " ";
    if (data === undefined) {
      data = [{ value: 1, title: "Temp1" }, { value: 2, title: "Temp2" }];
    }
    console.log("==", data);
    return (
      <div>
        <div className="input-group combo-box">
          <div className={labelClassName + " combo-box-label"}>{label}</div>
          <select
            onChange={e => {
              this.onChangeHandler(e);
            }}
            className={inputClass}
            value={value || ""}
            id={id}
            name={name}
          >
            {data.map(item => {
              return <option value={item.value}>{item.title}</option>;
            })}
          </select>
        </div>
        {errorText && (
          <div className="row input_error_container">
            <span className={labelClassName} />
            <div className="validate-title">{errorText}</div>
          </div>
        )}
      </div>
    );
  }
}
ComboBox.defaultProps = {
  inputClassName: "form-control col-6 col-md-6 col-sm-6 col-lg-6",
  labelClassName: "input-group-addon col-6 col-md-5 col-sm-5 col-lg-5"
};
export default ComboBox;
