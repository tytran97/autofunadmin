import React from "react";

import "./Loading.css";

import loadingIcon from "assets/images/loading-icon.gif";

const Loading = ({ type }) => {
  return (
    <div className="loading">
      <img src={loadingIcon} alt="loading" className="loading__icon" />
    </div>
  );
};
export default Loading;
