import React, { Component } from "react";
import Cookies from "js-cookie";
import { Route, Redirect } from "react-router-dom";

export default class PrivateRoute extends Component {
  render() {
    let { component: Component, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={props =>
          Cookies.get("accessToken") ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{ pathname: "/login", state: { from: props.location } }}
            />
          )
        }
      />
      
    );
  }
}
