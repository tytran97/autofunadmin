import React from "react";
import { withRouter, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import store from "store";
import Cookies from "js-cookie";
import swal from "sweetalert";
import "./Menu.css";

class Menu extends React.Component {
  _onLogout() {
    swal({
      title: "Bạn có muốn đăng xuất?",
      buttons: {
        Cancel: "cancel",
        OK: {
          text: "OK",
          value: "OK"
        }
      }
    }).then(value => {
      switch (value) {
        case "OK":
          Cookies.remove("accessToken");
          store.dispatch(push("/login"));
          break;
      }
    });
  }
  render() {
    return (
      <div className="left-sidebar">
        <div className="logo">
          <i id="logo" className={"fas fa-crow purun"} />
        </div>
        <div className="scroll-sidebar">
          <nav className="sidebar-nav">
            <ul id="sidebarnav">
              <li>
                <NavLink activeclassname="active" to="/car/list" exact>
                  Xe
                </NavLink>
              </li>
              <li>
                <NavLink activeclassname="active" to="/acce/list" exact>
                  Phụ kiện
                </NavLink>
              </li>
              <li>
                <NavLink activeclassname="active" to="/user/list" exact>
                  Người dùng
                </NavLink>
              </li>
              <li>
                <NavLink activeclassname="active" to="/bill/list" exact>
                  Đơn hàng
                </NavLink>
              </li>
              <li>
                <div className="logout-link" onClick={() => this._onLogout()}>
                  Đăng xuất
                </div>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Menu));

// export default withRouter(Menu);
