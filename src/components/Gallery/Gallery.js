import React from "react";
import "react-notifications/lib/notifications.css";
import { connect } from "react-redux";

import "./Gallery.css";
import { NotificationManager } from "react-notifications";
import delButton from "../../assets/images/close-button.png";
import { actions as Actions } from "reducers/carAdding/actions";

class Gallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonAdd: true,
      modified: false,
      selectedFile: [],
      imgs: []
    };
  }
  changeHandler = event => {
    let file = [];
    let temp = true;
    let { selectedFile, imgs } = this.state;
    file = Array.from(event.target.files);
    file.map(item => {
      if (selectedFile.length < 8) {
        if (item.size < 1048576) {
          selectedFile.push(item);
          let reader = new FileReader();
          reader.onload = e => {
            imgs.push(e.target.result);
            this.setState({ selectedFile, imgs });
          };
          reader.readAsDataURL(item);
        } else {
          NotificationManager.warning(
            "File " +
              item.name +
              " is too large (~" +
              item.size / 1048576 +
              "MB)",
            "Warning",
            300000
          );
        }
      } else {
        temp = false;
      }
    });
    if (!temp) NotificationManager.info("Only allow 8 images per car !");
    this.setState({
      buttonAdd: temp
    });
    this.props.setImage(this.state.selectedFile);
  };

  onClickImage = index => {
    let { selectedFile, imgs } = this.state;
    selectedFile = this.props.car.image;
    selectedFile.splice(index, 1);
    imgs.splice(index, 1);
    this.props.setImage(selectedFile);
    this.setState({ selectedFile, imgs });
    this.setState({
      buttonAdd: true
    });
  };

  render() {
    let { selectedFile, imgs } = this.state;
    let { errorText } = this.props;
    let disabled = this.state.buttonAdd;
    if (this.state.modified) {
      errorText = "";
    }
    disabled = this.state.buttonAdd;
    return (
      <div align="center">
        <div className=" gallery-menu">
          <div className="gallery-title">Hình ảnh xe</div>
          {disabled === false ? (
            <div />
          ) : (
            <label htmlFor="upload-img">
              <div className="lhtd-button btn-link">Thêm ảnh</div>
            </label>
          )}
          <input
            multiple
            type="file"
            id="upload-img"
            className="add-img"
            onClick={event => (event.target.value = null)}
            onChange={this.changeHandler}
          />
        </div>
        <div className="gallery">
          {selectedFile.map((f, index) => {
            return (
              <div className="img-row">
                <img className="img" key={index} src={imgs[index]} />
                <img
                  className="img-button"
                  src={delButton}
                  onClick={() => this.onClickImage(index)}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
Gallery.defaultProps = {
  inputClassName: "form-control col-6 col-md-6 col-sm-6 col-lg-6",
  labelClassName: "input-group-addon col-6 col-md-5 col-sm-5 col-lg-5"
};
const mapStateToProps = state => ({
  car: state.carAdding
});

const mapDispatchToProps = {
  ...Actions
};
export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
