import React from "react";

import "./Input.css";

class InputBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modified: false
    };
  }
  onChangeHandler(e) {
    let value = e.target.value;
    let buffer = value.split("");
    let last = buffer[buffer.length - 1];
    if (this.props.type === "number") {
      if (last >= 48 || last <= 57 || buffer.length === 0) {
        this.props.onChange(e);
      }
    } else {
      this.props.onChange(e);
    }
  }
  render() {
    let {
      label,
      value,
      name,
      id,
      errorText,
      inputClassName,
      labelClassName,
      disabled,
      maxLength,
      subtitle
    } = this.props;
    if (this.state.modified) {
      errorText = "";
    }
    let inputClass = inputClassName;
    inputClass += errorText ? " errorInput" : " ";
    return (
      <div>
        <div className="input-group input-box">
          <div className={labelClassName + " input-box-label"}>{label}</div>
          <input
            required="true"
            maxLength={maxLength}
            type={"text"}
            value={value || ""}
            id={id}
            name={name}
            className={inputClass}
            subtitle={subtitle}
            onChange={e => {
              this.onChangeHandler(e);
            }}
            disabled={disabled}
          />
          <div className="col-1 input_max_length">{subtitle}</div>
        </div>
        {errorText && (
          <div className="row input_error_container">
            <span className={labelClassName} />
            <div className="validate-title">{errorText}</div>
          </div>
        )}
      </div>
    );
  }
}
InputBox.defaultProps = {
  maxLength: 100,
  value: "",
  inputClassName: "form-control col-6 col-md-6 col-sm-6 col-lg-6",
  labelClassName: "input-group-addon col-6 col-md-5 col-sm-5 col-lg-5"
};
export default InputBox;
