import React from "react";
import { connect } from "react-redux";

import CustomPopup from "components/CustomPopup";
import {
  actions as infoTypeActions,
  status as infoTypeStatus
} from "reducers/infoType";

import "./TypeItem.css";

class TypeItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false
    };
    this.openModal = this.openModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.deleteInfoType = this.deleteInfoType.bind(this);
  }

  openModal(e) {
    e.stopPropagation();
    this.setState({ modalIsOpen: true });
  }

  onCloseModal = () => {
    this.setState({ modalIsOpen: false });
  };

  deleteInfoType = () => {
    this.props.hanlderDelete(this.props.data.id);
  };
  okBtnFunctionHandle() {
    if (this.props.infoType.deleteStatus === infoTypeStatus.SUCCESS) {
      this.onCloseModal(false);
      this.props.setDeleteStatus(null);
      this.props.setMessage({ error: {} });
      this.props.getInfoTypesAsync();
    } else if (this.props.infoType.deleteStatus === infoTypeStatus.ERROR) {
      this.onCloseModal(false);
      this.props.setDeleteStatus(null);
    } else {
      this.deleteInfoType();
    }
  }

  handldeOnClickItem(id) {
    this.props.onClickRowHandle(id);
  }
  render() {
    let { data, infoType, error, setDeleteStatus, setMessage } = this.props;
    const { modalIsOpen } = this.state;
    let isShowCancelBtn;
    let okBtnLabel;
    if (this.props.infoType.deleteStatus === infoTypeStatus.SUCCESS) {
      isShowCancelBtn = false;
    } else if (this.props.infoType.deleteStatus === infoTypeStatus.ERROR) {
      isShowCancelBtn = false;
      okBtnLabel = "一覧に戻る";
    }
    let messageE001 =
      "この情報タイプは使用中なので削除することはできません。先に関連情報を削除してください。";
    return (
      <React.Fragment>
        <div
          className="col-xs-6 co-sm-6 types-box"
          onClick={e => this.handldeOnClickItem(data.id)}
        >
          <div className="line2">
            <i className="far fa-trash-alt" onClick={this.openModal} />
          </div>
          <span className="info-type-id">
            ID : <b>{data.id}</b>
          </span>
          <img src={data.icon} alt="Responsive" />
          <p>{data.name}</p>
        </div>
        {
          <CustomPopup
            isOpen={modalIsOpen}
            message={"本当に削除しますか？"}
            errorMessage={error.code === "E001" ? messageE001 : null}
            status={infoType.deleteStatus}
            showCancelBtn={isShowCancelBtn}
            okBtnLabel={okBtnLabel}
            okBtnFunction={() => {
              this.okBtnFunctionHandle();
            }}
            onClose={() => {
              this.onCloseModal();
              setDeleteStatus(null);
              setMessage({ error: {} });
            }}
          />
        }
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  infoType: state.infoType,
  error: state.infoType.error
});

const mapDispatchToProps = {
  getInfoTypesAsync: infoTypeActions.getInfoTypesAsync,
  setDeleteStatus: infoTypeActions.setDeleteStatus,
  setMessage: infoTypeActions.setMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(TypeItem);
