import React from "react";

import "./SelectMenuRow.css";

export default ({ index, content, onDeleteInfoMenu }) => {
  return (
    <div className="form-group row">
      <div className="col-md-4 text-right">
        {index === 0 ? "選択されたサブメニュー" : ""}
      </div>
      <div className="col-md-4 col-border">
        <div>{content.name}</div>
      </div>
      <span className="text-menuId">
        {content.id
          ? ["メニューID: ", <b key={content.id}>{content.id}</b>]
          : ""}&ensp;&ensp;
      </span>
      <div className="col-md-1">
        <a
          className="select-menu-delete-cursor"
          onClick={() => onDeleteInfoMenu(content)}
        >
          <i className="far fa-trash-alt custom-fa" />
        </a>
      </div>
    </div>
  );
};
