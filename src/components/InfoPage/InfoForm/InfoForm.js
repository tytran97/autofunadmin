import React from "react";

import SelectTypeRow from "../SelectTypeRow";
import loadingIcon from "assets/images/loading-icon.gif";
import "./InfoForm.css";

export default ({
  info,
  tabs,
  currentTab,
  types,
  handleCurrentTypeClick,
  handleChange
}) => {
  let tab = currentTab;
  let maxLength = 100;
  let olength = 0;

  return (
    <React.Fragment>
      <div className={`row form-group ${info.id ? "" : "hiden"}`}>
        <label className="col-md-4 col-form-label">情報ID</label>
        <div className="col-md-5 col-sm-12 col-xs-12 ">
          <input className="form-control" value={info.id} disabled readOnly />
        </div>
      </div>
      <div className="form-group row">
        <label className="col-md-4 col-form-label" style={{ paddingTop: 15 }}>
          情報タイプ
        </label>
        <div id="selectinfotype" className="col-md-8">
          {tabs.map((content, index) => (
            <div
              title={content.name}
              key={index}
              className={`form-type ${content.active} ${
                content.currentTab === tab ? "form-type-color" : ""
              }  `}
              onClick={() => handleCurrentTypeClick(content.typeId, currentTab)}
            >
              <i className={`fa ${content.icon} fa-icon`} />
              <p>{content.name}</p>
            </div>
          ))}
        </div>
      </div>

      <div className={`row form-group ${tab === "text" ? "hiden" : ""}`}>
        <label htmlFor="infoTitle" className="col-md-4 col-form-label">
          {(tab === "link" || tab === "phone") && <span>タイトル</span>}
        </label>
        <div className="col-md-5 col-sm-12 col-xs-12 ">
          <input
            maxLength="100"
            className={`form-control ${info.error.title ? "has-error" : ""}`}
            type="text"
            id="title"
            name="title"
            value={info.title ? info.title : ""}
            onChange={e => handleChange(e.target.name, e.target.value)}
          />

          <span className="validate-title">{info.error.title}</span>
        </div>
        <label className="text-rate">
          {info.title ? info.title.length : olength}/{maxLength}
        </label>
      </div>
      <div className="row form-group">
        <label htmlFor="infoValue" className="col-md-4 col-form-label">
          {tab === "link" && <span>URLリンク</span>}{" "}
          {tab === "phone" && <span>電話番号</span>}{" "}
          {tab === "text" && <span>説明⽂</span>}
        </label>
        <div className="col-md-5">
          <input
            maxLength="100"
            className={`form-control ${info.error.value ? "has-error" : ""}`}
            type="text"
            id="infoValue"
            name="value"
            value={info.value ? info.value : ""}
            onChange={e => handleChange(e.target.name, e.target.value)}
          />
          <span className="validate-title">{info.error.value}</span>
        </div>
        {(tab === "link" || tab === "text") && (
          <label className={`text-rate ${tab === "phone" ? "hiden" : ""}`}>
            {info.value ? info.value.length : olength}/{maxLength}
          </label>
        )}
      </div>
      <div className={`row form-group ${tab === "link" ? "" : "hiden"}`}>
        <label className="col-md-4 col-form-label">表示するアイコン</label>
        {types.status === "LOADING" && (
          <div className="col-md-8 col-select-info-type">
            <div className="form-type form-display-icon">
              <img src={loadingIcon} width="35" alt="Responsive" />
            </div>
          </div>
        )}

        {types.status === "SUCCESS" && (
          <React.Fragment>
            <div className="col-md-7 col-select-info-type">
              {types.contents.map((content, index) => {
                return (
                  <SelectTypeRow
                    handleIconClick={handleCurrentTypeClick}
                    typeId={info.typeId}
                    key={index}
                    content={content}
                  />
                );
              })}
            </div>
            <span
              className={`${
                info.error.typeId ? "validate-title " : " "
              }text-type`}
            >
              {info.typeId
                ? ["情報タイプID: ", <b key={info.typeId}>{info.typeId}</b>]
                : ""}
              {info.error.typeId}
            </span>
          </React.Fragment>
        )}
      </div>
    </React.Fragment>
  );
};
