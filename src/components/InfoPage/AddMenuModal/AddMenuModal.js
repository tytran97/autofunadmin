import React, { Component } from "react";
import { connect } from "react-redux";
import ReactPaginate from "react-paginate";

import Modal from "react-responsive-modal/lib/css";
import "react-responsive-modal/lib/react-responsive-modal.css";
import "./AddMenuModal.css";

import SubmenuTable from "../SubmenuTable";
import TopMenu from "../TopMenu";
import SearchBar from "../../SearchBar";
import {
  actions as menuActions,
  status as menuStatus
} from "../../../reducers/menu";
import { actions as modalActions } from "../../../reducers/modal";
import { actions as infoActions } from "reducers/info";

class AddMenuModal extends Component {
  constructor(props) {
    super(props);
    this.handleMenuRowClick = this.handleMenuRowClick.bind(this);
    this.handleTopMenuClick = this.handleTopMenuClick.bind(this);
    this.removeCurrentMenu = this.removeCurrentMenu.bind(this);
    this.searchMenu = this.searchMenu.bind(this);
    this.state = {
      currentMenuList: null
    };
  }

  componentDidMount() {
    let { currentMenus } = this.props;
    this.setState({ currentMenuList: currentMenus.slice() });
  }

  handleMenuRowClick(menu) {
    let currentMenuList = this.state.currentMenuList
      ? this.state.currentMenuList.slice()
      : null;
    currentMenuList.push(menu);
    this.setState({ currentMenuList });
  }

  removeCurrentMenu(menu) {
    let currentMenuList = this.state.currentMenuList.slice();
    let index;
    for (let i = 0; i < currentMenuList.length; i++) {
      if (currentMenuList[i].id === menu.id) {
        index = i;
      }
    }
    currentMenuList.splice(index, 1);
    this.setState({ currentMenuList: currentMenuList });
  }

  addCurrentMenu() {
    let { currentMenuList } = this.state;
    let { currentTopMenuId } = this.props;
    this.props.setCurrentMenu(currentMenuList);
    this.props.setSubmenuCurrentSearchKeyword("", currentTopMenuId);
    this.props.setSubmenuCurrentTablePage(1, currentTopMenuId);
    this.props.setOpenModal(false);
  }

  menuCancelBtn() {
    let currentMenus = this.props.currentMenus.slice();
    let { currentTopMenuId } = this.props;
    this.setState({ currentMenuList: currentMenus });
    this.props.setSubmenuCurrentSearchKeyword("", currentTopMenuId);
    this.props.setSubmenuCurrentTablePage(1, currentTopMenuId);
    this.props.setOpenModal(false);
  }

  handleTopMenuClick(currentTopMenuId) {
    let { currentSearchKeyword } = this.props;
    this.props.getSubmenusAsync(currentTopMenuId, currentSearchKeyword);
  }

  searchSuggestion(keyword) {
    let { currentTopMenuId } = this.props;
    this.props.searhSuggestion(keyword, currentTopMenuId);
  }
  searchMenu(keyword) {
    let { currentTopMenuId } = this.props;
    this.props.getSubmenusAsync(currentTopMenuId, keyword);
  }

  render() {
    const {
      currentTopMenuId,
      currentSumenuSearchKeyword,
      currentSubmenuTablePage,
      currentResultSearchSuggestion,
      topMenu,
      submenu,
      status,
      isOpen,
      searchStatus,
      total
    } = this.props;
    let DEFAULT_PAGINATION_LIMIT = 5;
    let { currentMenuList } = this.state;
    let { setOpenModal, setSubmenuCurrentTablePage } = this.props;
    let { getSubmenusAsync } = this.props;
    let pageCount = total / DEFAULT_PAGINATION_LIMIT;
    let searchPlaceholder = "サブメニューを検索";
    return (
      <div className="row">
        <button
          onClick={() => {
            this.props.setSubmenuCurrentSearchKeyword("");
            setOpenModal(true);
            this.searchMenu("");
            setSubmenuCurrentTablePage(1);
            this.setState({ currentMenuList: this.props.currentMenus });
          }}
          className="lhtd-button btn-open-modal"
        >
          <i className="fa fa-plus-circle" />
          <span> メニューを追加</span>
        </button>

        <Modal
          open={isOpen}
          onClose={() => {
            setOpenModal(false);
          }}
          closeOnOverlayClick={false}
          closeOnEsc={false}
          little
          classNames={{
            overlay: "custom-overlay-menu",
            modal: "custom-modal-menu"
          }}
        >
          <div className="modal-menu">
            <h4>情報を表示する場所を選択</h4>
            <div className="row modal-menu-container">
              <div className="modal-top-menu">
                <p>トップメニュー</p>
                <TopMenu
                  topMenu={topMenu}
                  currentTopMenuId={currentTopMenuId}
                  currentMenuList={currentMenuList}
                  removeMenu={this.removeCurrentMenu}
                  handleTopMenuClick={this.handleTopMenuClick}
                  handleMenuClick={this.handleMenuRowClick}
                />
              </div>
              <div className="modal-submenu">
                <p>サブメニュー</p>
                <div className="modal-submenu-container">
                  <SearchBar
                    resultSearchSuggestion={currentResultSearchSuggestion}
                    placeholder={searchPlaceholder}
                    btnSearchOnclick={keyword => {
                      this.props.setSubmenuCurrentSearchKeyword(keyword);
                      this.searchMenu(keyword);
                      setSubmenuCurrentTablePage(1);
                    }}
                    handleSearchSuggestion={e => this.searchSuggestion(e)}
                    showSearchBtn={true}
                    status={searchStatus}
                  />

                  <SubmenuTable
                    pageCount={pageCount}
                    status={status}
                    menuStatus={menuStatus}
                    submenu={submenu}
                    currentMenuList={currentMenuList}
                    removeMenu={this.removeCurrentMenu}
                    handleMenuClick={this.handleMenuRowClick}
                  />

                  {status === menuStatus.ERROR && (
                    <div>時間をおいて再度お試しください</div>
                  )}
                  {pageCount > 0 && (
                    <div className="row modal-paginate-menu">
                      <ReactPaginate
                        previousLabel={"<"}
                        nextLabel={">"}
                        breakLabel={<a href="">...</a>}
                        breakClassName={"break-me"}
                        pageCount={pageCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        forcePage={currentSubmenuTablePage - 1}
                        onPageChange={page => {
                          let selected = page.selected + 1;
                          setSubmenuCurrentTablePage(selected);
                          getSubmenusAsync(
                            currentTopMenuId,
                            currentSumenuSearchKeyword,
                            (selected - 1) * DEFAULT_PAGINATION_LIMIT
                          );
                        }}
                        containerClassName={"pagination"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}
                      />
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="row btn-sumbit">
              <button
                className="lhtd-button custom-lhtd-button"
                onClick={() => this.addCurrentMenu()}
              >
                追加
              </button>
              <button
                className="lhtd-button btn-menu-cancel"
                onClick={() => this.menuCancelBtn()}
              >
                キャンセル
              </button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentTopMenuId: state.menu.currentTopMenuId,
  currentSubmenuTablePage: state.menu.currentSubmenuTablePage,
  currentSumenuSearchKeyword: state.menu.currentSumenuSearchKeyword,
  currentResultSearchSuggestion: state.menu.currentResultSearchSuggestion,
  searchStatus: state.menu.searchStatus,
  status: state.menu.status,
  submenu: state.menu.submenus,
  topMenu: state.menu.topMenus,
  currentMenus: state.info.currentMenus,
  total: state.menu.totalSubmenu,
  isOpen: state.modal.isOpen
});

const mapDispatchToProps = {
  setCurrentMenu: infoActions.setCurrentMenu,
  getSubmenusAsync: menuActions.getSubmenusAsync,
  setSubmenuCurrentTablePage: menuActions.setSubmenuCurrentTablePage,
  setSubmenuCurrentSearchKeyword: menuActions.setSubmenuCurrentSearchKeyword,
  searhSuggestion: menuActions.searchSuggestionAsync,
  setOpenModal: modalActions.setOpenModal
};

export default connect(mapStateToProps, mapDispatchToProps)(AddMenuModal);
