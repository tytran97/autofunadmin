import React from "react";

import "./TopMenu.css";

export default ({
  topMenu,
  currentTopMenuId,
  currentMenuList,
  handleTopMenuClick,
  removeMenu,
  handleMenuClick
}) => {
  let currentMenuIdList = currentMenuList
    ? currentMenuList.map(menu => menu.id)
    : null;
  return (
    <div className="">
      {topMenu && (
        <ul>
          {topMenu.map((content, index) => {
            return (
              <li
                className={`${content.id === currentTopMenuId ? "active" : ""}`}
                key={index}
              >
                <a
                  className="top-menu-cursor"
                  onClick={() => handleTopMenuClick(content.id)}
                >
                  {content.icon ? (
                    <img src={content.icon} alt="req" width="20" height="20" />
                  ) : (
                    <span />
                  )}
                  <span className={content.icon ? "" : "menu-img"}>
                    {content.name}
                  </span>
                </a>
                <div className="row-top-menu-tick">
                  {currentMenuIdList &&
                  currentMenuIdList.find(function(id) {
                    return id === content.id;
                  }) ? (
                    <div>
                      <i
                        onClick={() => removeMenu(content)}
                        className="fa fa-check custom-fa__top-menu"
                      />
                    </div>
                  ) : (
                    <div
                      className="top-menu-cursor"
                      onClick={() => handleMenuClick(content)}
                    >
                      <i className="fa fa-plus-circle" />
                    </div>
                  )}
                </div>
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
};
