import React from "react";

import "./SubmenuTable.css";
import Loading from "../../Loading";

export default ({
  submenu,
  status,
  menuStatus,
  currentMenuList,
  handleMenuClick,
  removeMenu,
  pageCount,
  header
}) => {
  let currentMenuIdList = currentMenuList
    ? currentMenuList.map(menu => menu.id)
    : null;
  return (
    <div className="form-group row">
      <table className="table table-fixed table-custom-border">
        <thead>
          <tr>
            <th>サブメニュー一覧</th>
          </tr>
        </thead>
        {pageCount > 0 && (
          <tbody>
            {submenu.map((content, index) => (
              <tr key={content.id} className="click">
                {currentMenuIdList &&
                currentMenuIdList.find(function(id) {
                  return id === content.id;
                }) ? (
                  <td className="active">
                    {content.name}
                    <span className="row-top-menu-tick">
                      <i
                        onClick={() => removeMenu(content)}
                        className="fa fa-check fa-custom-icon"
                      />
                    </span>
                  </td>
                ) : (
                  <td
                    className="click"
                    onClick={() => handleMenuClick(content)}
                  >
                    {content.name}
                    <span className="row-top-menu-tick">
                      <i className="fa fa-plus-circle" />
                    </span>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        )}
      </table>
      {status === menuStatus.LOADING && (
        <div className="loading">
          <Loading />
        </div>
      )}

      {pageCount === 0 &&
        status === menuStatus.SUCCESS && (
          <div style={{ marginLeft: 16 }}>該当する件は存在しません</div>
        )}
    </div>
  );
};
