import React from "react";

import "./SelectTypeRow.css";

export default ({ typeId, content, handleIconClick }) => {
  return (
    <div
      title={content.name}
      className={`form-type form-display-icon ${
        content.name === "電話番号" || content.name === "説明文"
          ? "type-hiden "
          : ""
      } ${content.id === typeId ? "form-type-color" : ""}`}
      onClick={() => handleIconClick(content.id)}
    >
      <img src={content.icon} alt="rep" width="25" height="25" />
      <div>{content.name}</div>
    </div>
  );
};
