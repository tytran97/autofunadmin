import React from "react";
import PropTypes from "prop-types";
import Modal from "react-responsive-modal";

import loadingIcon from "assets/images/loading-icon.gif";

import "./popup.css";

const loadingStyle = {
  backgroundImage: "url(" + loadingIcon + ")"
};

class CustomPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isClosing: false };
  }
  onClose() {
    this.setState({ isClosing: true }, () => this.props.onClose());
  }
  componentWillUpdate() {
    if (this.state.isClosing && !this.props.status)
      this.setState({ isClosing: false });
  }
  render() {
    let {
      message,
      showCancelBtn,
      showOkBtn,
      status,
      isOpen,
      okBtnFunction,
      okBtnLabel,
      cancelBtnLabel
    } = this.props;
    let title = " Thông báo";
    let titleIcon = "far fa-question-circle";
    switch (status) {
      case "SUCCESS":
        title = " 成功";
        titleIcon = "far fa-check-circle";
        break;
      case "ERROR":
        title = " エラー";
        titleIcon = "fas fa-exclamation-circle";
        //message = errorMessage || "処理中にエラーが発生しました。";
        break;
      default:
        break;
    }

    return (
      <Modal
        closeOnOverlayClick={false}
        closeOnEsc={false}
        open={isOpen}
        onClose={() => this.onClose()}
        little
        classNames={{
          overlay: "custom-overlay__popup",
          modal: "custom-modal__popup",
          closeIcon: "custom-icon"
        }}
      >
        {!this.state.isClosing && (
          <div className="popup-container">
            <div className="modal-header">
              <i className={titleIcon}>{title}</i>
            </div>
            {status === "LOADING" ? (
              <div style={loadingStyle} className=" popup-loading">
                {/* <p>{loadMessage}</p> */}
              </div>
            ) : (
              <div>
                <div className="modal-message-container form-control">
                  {message}
                </div>
                <div
                  className={
                    showCancelBtn && showOkBtn
                      ? "modal-btn-container"
                      : "modal-btn-container flex_center"
                  }
                >
                  {showCancelBtn && (
                    <button
                      // className={`${classCancel ? classCancel : "ldth-NO"}`}
                      onClick={() => this.onClose()}
                    >
                      {cancelBtnLabel}
                    </button>
                  )}
                  {showOkBtn && (
                    <button className="ldth-OK" onClick={okBtnFunction}>
                      {okBtnLabel}
                    </button>
                  )}
                </div>
              </div>
            )}
          </div>
        )}
      </Modal>
    );
  }
}

CustomPopup.propTypes = {
  isOpen: PropTypes.bool,
  okBtnFunction: PropTypes.func,
  onClose: PropTypes.func,
  message: PropTypes.string,
  cancelBtnLabel: PropTypes.string,
  okBtnLabel: PropTypes.string
};

CustomPopup.defaultProps = {
  okBtnLabel: "OK",
  cancelBtnLabel: "Hủy bỏ",
  loadMessage: "Thông báo",
  showOkBtn: true,
  showCancelBtn: true
};
export default CustomPopup;
