import React from "react";

import "./AreaText.css";

class AreaText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modified: false
    };
  }
  onChangeHandler(e) {
    this.props.onChange(e);
  }
  render() {
    let {
      value,
      name,
      id,
      errorText,
      inputClassName,
      labelClassName,
      maxLength
    } = this.props;
    if (this.state.modified) {
      errorText = "";
    }
    let inputClass = inputClassName;
    inputClass += errorText ? " errorInput" : " ";
    return (
      <div>
        <div>
          <textarea
            maxLength={maxLength}
            id={id}
            name={name}
            value={value || ""}
            className={inputClass + "input-area"}
            placeholder=" Enter description here !"
            onChange={e => {
              this.onChangeHandler(e);
            }}
          />
          <div className="col-1 area_max_length">
            {(value ? value.length : "0") + "/" + maxLength}
          </div>
        </div>
        {errorText && (
          <div className="row input_error_container">
            <span className={labelClassName} />
            <div className="validate-title">{errorText}</div>
          </div>
        )}
      </div>
    );
  }
}
AreaText.defaultProps = {
  maxLength: 500,
  value: "",
  inputClassName: "",
  labelClassName: "input-group-addon col-6 col-md-5 col-sm-5 col-lg-5"
};
export default AreaText;
