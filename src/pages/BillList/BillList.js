import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";
import { push } from "react-router-redux";
import store from "store";
import "./BillList.css";

import DataTable from "components/DataTable";
import SearchBar from "components/SearchBar";
import Menu from "components/Menu";

import { actions as Actions } from "reducers/billList/actions";

class BillList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowDeletedIndex: 0
    };
    let accessToken = Cookies.get("accessToken");
    if (!accessToken) {
      store.dispatch(push("/login"));
    }
    this.props.searchAsync();
  }

  render() {
    return (
      <React.Fragment>
        <div className="header">
          <div className="topbar">
            <div className="row">
              <div className="col-md-5 header-titles-left">LongK</div>
              {/* <div className="col-md-7 float-right btn-add">
                <Link to="/acce/create" className="lhtd-button btn-link">
                  <i className="far fa-plus-square" />
                  <span> Thêm phụ kiện </span>
                </Link>
              </div> */}
            </div>
          </div>
        </div>
        <div className="content">
          <Menu />
          <div className="page-wrapper">
            <div className="container-fluid">
              <div className="row search-bar">
                <SearchBar
                  type="billList"
                  ref={ref => (this.searchBar = ref)}
                />
              </div>
              <DataTable type="billList" />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  item: state.billList
});

const mapDispatchToProps = {
  ...Actions
};

export default connect(mapStateToProps, mapDispatchToProps)(BillList);
