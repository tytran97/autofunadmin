import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Cookies from "js-cookie";
import { push } from "react-router-redux";
import store from "store";

import "./Login.css";
import { actions as authInfoActions } from "reducers/authInfo/index";
import loadingIcon from "assets/images/loading-icon.gif";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    let accessToken = Cookies.get("accessToken");
    if (accessToken) {
      store.dispatch(push("/car/list"));
    }
  }

  handleLogin = e => {
    e.preventDefault();
    this.props.login(this.state.username, this.state.password);
  };

  render() {
    let loginButtonContent = "Đăng nhập";
    let { status } = this.props;
    let isLoading = status === "loading";
    let isSuccess = status === "success";
    let isError = status === "error";
    if (isSuccess) {
      return (
        <Redirect
          to={{
            pathname: "/car/list"
          }}
        />
      );
    } else if (isLoading) {
      loginButtonContent = (
        <img src={loadingIcon} alt="loading" style={{ width: "18px" }} />
      );
    }
    return (
      <div className="login-wrapper">
        <form
          onSubmit={this.handleLogin}
          className={"login-group " + (isError ? "animated shake" : "")}
        >
          <div className="lxl-title">Admin login</div>
          <div className="login-input-group">
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input
                id="username"
                required
                onChange={e => {
                  this.setState({ username: e.target.value });
                }}
                className="form-control"
                type="text"
                spellCheck={false}
                disabled={isLoading}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                id="password"
                required
                onChange={e => {
                  this.setState({ password: e.target.value });
                }}
                className="form-control "
                type="password"
                spellCheck={false}
                disabled={isLoading}
                required
              />
            </div>
          </div>
          <button
            type="submit"
            className={
              "lhtd-button lxl-login-button " +
              (isLoading ? "lxl-login-button--loading" : "")
            }
          >
            {loginButtonContent}
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  status: state.authInfo.status
});

const mapDispatchToProps = {
  login: authInfoActions.getLoginAsync
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
