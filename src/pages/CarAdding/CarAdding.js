import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";
import { push } from "react-router-redux";
import store from "store";
import "./CarAdding.css";

import backButton from "../../assets/images/chevron-left.png";
import { actions as Actions } from "reducers/carAdding/actions";
import InputBox from "../../components/InputBox/Input";
import ComboBox from "../../components/ComboBox";
import AreaText from "../../components/AreaText";
import Gallery from "../../components/Gallery";

class CarAdding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      brandList: [],
      gearList: []
    };
    this.props.getAllList();
    let accessToken = Cookies.get("accessToken");
    if (!accessToken) {
      store.dispatch(push("/login"));
    }
  }

  _onPost() {
    this.props.addCar(this.props.car);
  }

  render() {
    const yearList = [];
    const thisYear = new Date().getFullYear();
    for (let i = thisYear; i >= 2005; i--) {
      yearList.push({ value: i, title: i });
    }
    let { car } = this.props;
    return (
      <React.Fragment>
        <div className="header">
          <div className="topbar">
            <div className="row">
              <div className=" header-adding">
                <Link to="/car/list">
                  <img src={backButton} />
                </Link>
                <div className="menu-title">Thêm Xe Mới</div>
                <div className="col-md-7 float-right btn-add">
                  <button className="lhtd-button btn-link">
                    <i className="far fa-plus-square" />
                    <span> Lưu </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="cell-content">
          <div>
            <div className="container-fluid left-content" align="center">
              <div className="cell-title">Thông Số Cơ Bản</div>
              <div className="input-cell">
                <InputBox
                  label="Tên Xe: "
                  id="name"
                  value={car.name}
                  type="text"
                  onChange={e => this.props.setName(e.target.value)}
                />

                <ComboBox
                  label="Hãng Xe:"
                  id="type"
                  value={car.brand}
                  onChange={e => {
                    this.props.setBrand(e.target.value);
                  }}
                  data={car.brandList}
                />

                <ComboBox
                  label="Năm sản xuất:"
                  id="year"
                  value={car.year}
                  onChange={e => this.props.setYear(e.target.value)}
                  data={yearList}
                />

                <ComboBox
                  label="Xuất xứ:"
                  id="origin"
                  value={car.origin}
                  onChange={e => this.props.setOrigin(e.target.value)}
                  data={[
                    { value: 1, title: "Nhập Khẩu" },
                    { value: 2, title: "Lắp ráp trong nước" }
                  ]}
                />

                <ComboBox
                  label="Tình trạng:"
                  id="status"
                  value={car.status}
                  onChange={e => this.props.setStatus(e.target.value)}
                  data={[
                    { value: 1, title: "Xe mới" },
                    { value: 2, title: "Xe đã dùng" }
                  ]}
                />

                <ComboBox
                  label="Dòng xe:"
                  id="kind"
                  value={car.carType}
                  onChange={e => this.props.setCarType(e.target.value)}
                  data={[
                    { value: 1, title: "Sedan" },
                    { value: 2, title: "SUV" },
                    { value: 3, title: "Couple" },
                    { value: 4, title: "Crossover" },
                    { value: 5, title: "Hatchback" },
                    { value: 6, title: "Convertible/Cabriolet" },
                    { value: 7, title: "Truck" },
                    { value: 8, title: "Wagon" }
                  ]}
                />

                <InputBox
                  label="Số Km đã đi: "
                  id="km"
                  value={car.numOfKm}
                  type="number"
                  subtitle="Km"
                  onChange={e => this.props.setNumOfKm(e.target.value)}
                />

                <InputBox
                  label="Giá Tiền: "
                  id="price"
                  value={car.price}
                  type="number"
                  subtitle="Triệu"
                  onChange={e => this.props.setPrice(e.target.value)}
                />

                <ComboBox
                  label="Sơn ngoại thất:"
                  id="outerColor"
                  value={car.outerColor}
                  onChange={e => this.props.setOuterColor(e.target.value)}
                  data={[
                    { value: 1, title: "Xanh" },
                    { value: 2, title: "Đỏ" },
                    { value: 3, title: "Vàng" },
                    { value: 4, title: "Bạc" },
                    { value: 5, title: "Đen" },
                    { value: 6, title: "Xám" }
                  ]}
                />

                <ComboBox
                  label="Sơn nội thất:"
                  id="innerColor"
                  value={car.innerColor}
                  onChange={e => this.props.setInnerColor(e.target.value)}
                  data={[
                    { value: 1, title: "Xanh" },
                    { value: 2, title: "Đỏ" },
                    { value: 3, title: "Vàng" },
                    { value: 4, title: "Bạc" },
                    { value: 5, title: "Đen" },
                    { value: 6, title: "Xám" }
                  ]}
                />

                <InputBox
                  label="Số cửa:"
                  id="door"
                  value={car.numOfDoor}
                  type="number"
                  onChange={e => this.props.setNumOfDoor(e.target.value)}
                />

                <InputBox
                  label="Số chỗ ngồi:"
                  id="seat"
                  value={car.numOfSeat}
                  type="number"
                  onChange={e => this.props.setNumOfSeat(e.target.value)}
                />
              </div>
            </div>
            <div className="container-fluid right-content" align="center">
              <div className="cell-title">Hộp Số Chuyển Động</div>
              <div className="input-cell">
                <ComboBox
                  label="Hộp Số:"
                  id="gearBoxes"
                  value={car.gear}
                  onChange={e => this.props.setGear(e.target.value)}
                  data={car.gearList}
                />

                <ComboBox
                  label="Dẫn Động:"
                  id="actutor"
                  value={car.actutor}
                  onChange={e => this.props.setActutor(e.target.value)}
                  data={[
                    { value: 1, title: "FWD - Dẫn động cầu trước" },
                    { value: 2, title: "RWD - Dẫn động cầu sau" },
                    { value: 3, title: "4WD - Dẫn động 4 bánh" },
                    { value: 4, title: "AWD - 4 bánh toàn thời gian" }
                  ]}
                />
              </div>
              <div className="cell-title">Nhiên Liệu</div>
              <div className="input-cell">
                <ComboBox
                  label="Nhiên Liệu:"
                  id="fuel"
                  value={car.fuel}
                  onChange={e => this.props.setFuel(e.target.value)}
                  data={[
                    { value: 1, title: "Xăng" },
                    { value: 2, title: "Hybrid" },
                    { value: 3, title: "Diesel" },
                    { value: 4, title: "Điện" }
                  ]}
                />

                <InputBox
                  label="Hệ thống nạp:"
                  id="fuelSys"
                  value={car.fuelSystem}
                  type="text"
                  onChange={e => this.props.setFuelSystem(e.target.value)}
                />

                <InputBox
                  label="Mức tiêu thụ:"
                  id="fuelConsumption"
                  value={car.fuelComsumption}
                  type="number"
                  subtitle="L/100km"
                  onChange={e => this.props.setFuelComsumption(e.target.value)}
                />
              </div>
              <div className="cell-title">Mô tả chi tiết</div>
              <div>
                <AreaText
                  label=""
                  id="description"
                  value={car.description}
                  onChange={e => this.props.setDescription(e.target.value)}
                />
              </div>
            </div>
          </div>
          <div className="container-fluid up-content ">
            <Gallery />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  car: state.carAdding
});

const mapDispatchToProps = {
  ...Actions
};

export default connect(mapStateToProps, mapDispatchToProps)(CarAdding);
