import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";
import { push } from "react-router-redux";
import store from "store";
import "./AcceAdding.css";

import backButton from "../../assets/images/chevron-left.png";
import { actions as Actions } from "reducers/acceAdding/actions";
import InputBox from "../../components/InputBox/Input";
import ComboBox from "../../components/ComboBox";
import AreaText from "../../components/AreaText";
import Gallery from "../../components/Gallery";

class AcceAdding extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    let accessToken = Cookies.get("accessToken");
    if (!accessToken) {
      store.dispatch(push("/login"));
    }
    this.props.getAllList();
  }

  _onPost() {
    this.props.addAcce(this.props.acce);
  }

  render() {
    let { acce } = this.props;
    return (
      <React.Fragment>
        <div className="header">
          <div className="topbar">
            <div className="row">
              <div className=" header-adding">
                <Link to="/acce/list">
                  <img src={backButton} />
                </Link>
                <div className="menu-title">Thêm Phụ Kiện Mới</div>
                <div className="col-md-7 float-right btn-add">
                  <button className="lhtd-button btn-link">
                    <i className="far fa-plus-square" />
                    <span> Lưu </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="cell-content">
          <div>
            <div className="container-fluid left-content" align="center">
              <div className="cell-title">Thông Số Cơ Bản</div>
              <div className="input-cell">
                <InputBox
                  label="Tên Xe: "
                  id="name"
                  value={acce.name}
                  type="text"
                  onChange={e => this.props.setName(e.target.value)}
                />

                <ComboBox
                  label="Loại Phụ Kiện:"
                  id="kind"
                  value={acce.acceType}
                  onChange={e => this.props.setAcceType(e.target.value)}
                  data={acce.typeList}
                />

                <ComboBox
                  label="Hãng sản xuất:"
                  id="type"
                  value={acce.brand}
                  onChange={e => {
                    this.props.setBrand(e.target.value);
                  }}
                  data={acce.brandList}
                />

                <InputBox
                  label="Số Lượng: "
                  id="quantity"
                  value={acce.quantity}
                  type="number"
                  onChange={e => this.props.setQuantity(e.target.value)}
                />

                <InputBox
                  label="Giá Tiền: "
                  id="price"
                  value={acce.price}
                  type="number"
                  subtitle="Trăm"
                  onChange={e => this.props.setPrice(e.target.value)}
                />
              </div>
            </div>
            <div className="container-fluid right-content" align="center">
              <div className="cell-title">Mô tả chi tiết</div>
              <div>
                <AreaText
                  label=""
                  id="description"
                  value={acce.description}
                  onChange={e => this.props.setDescription(e.target.value)}
                />
              </div>
            </div>
          </div>
          <div className="container-fluid ">
            <Gallery />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  acce: state.acceAdding
});

const mapDispatchToProps = {
  ...Actions
};

export default connect(mapStateToProps, mapDispatchToProps)(AcceAdding);
