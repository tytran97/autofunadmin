import Cookies from "js-cookie";
import { push } from "react-router-redux";
import store from "store";

export function get(url) {
  // let accessToken = Cookies.get("accessToken");
  const getUrl = new URL(url);
  // getUrl.searchParams.set("token", accessToken);
  return fetch(getUrl, {
    headers: {
      Accept: "application/json"
    }
  }).then(res => {
    if (res.status === 401) {
      // Cookies.remove("accessToken");
      store.dispatch(push("/login"));
    }
    return res;
  });
}

export function del(url) {
  let accessToken = Cookies.get("accessToken");
  return fetch(url, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      Authorization: "Bearer " + accessToken
    }
  }).then(res => {
    if (res.status === 401) {
      Cookies.remove("accessToken");
      store.dispatch(push("/login"));
    }
    return res;
  });
}

export function post(url, body, csvFlag = false) {
  let accessToken = Cookies.get("accessToken");
  let header = {};
  if (csvFlag) {
    header = {
      Authorization: "Bearer " + accessToken
    };
  } else {
    header = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + accessToken
    };
  }
  return fetch(url, {
    method: "POST",
    headers: header,
    body: body
  }).then(res => {
    if (res.status === 401) {
      Cookies.remove("accessToken");
      store.dispatch(push("/login"));
    }
    return res;
  });
}

export function postFormData(url, body) {
  let accessToken = Cookies.get("accessToken");
  let header = {};
  header = {
    Authorization: "Bearer " + accessToken
  };
  return fetch(url, {
    method: "POST",
    headers: header,
    body: body
  }).then(res => {
    if (res.status === 401) {
      Cookies.remove("accessToken");
      store.dispatch(push("/login"));
    }
    return res;
  });
}

export function put(url, body) {
  let accessToken = Cookies.get("accessToken");
  return fetch(url, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + accessToken
    },
    body: body
  }).then(res => {
    if (res.status === 401) {
      Cookies.remove("accessToken");
      store.dispatch(push("/login"));
    }
    return res;
  });
}
