import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { combineReducers } from "redux";
import createHistory from "history/createBrowserHistory";
import { routerReducer, routerMiddleware } from "react-router-redux";

import authInfo from "./reducers/authInfo";
import item from "./reducers/item";
import carList from "./reducers/carList";
import userList from "./reducers/userList";
import acceList from "./reducers/acceList";
import carAdding from "./reducers/carAdding";
import acceAdding from "./reducers/acceAdding";
import billList from "./reducers/billList";

const initialState = {};
const enhancers = [];

// Create a history of your choosing (we're using a browser history in this case)
export const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const historyMiddleware = routerMiddleware(history);

const middleware = [thunk, historyMiddleware];

const rootReducer = combineReducers({
  router: routerReducer,
  authInfo,
  item,
  carList,
  userList,
  acceList,
  carAdding,
  acceAdding,
  billList
});

// if (process.env.NODE_ENV === "development") {
//   const devToolsExtension =
//     window.__REDUX_DEVTOOLS_EXTENSION__ &&
//     window.__REDUX_DEVTOOLS_EXTENSION__();
//   enhancers.push(devToolsExtension);
// }

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

const store = createStore(rootReducer, initialState, composedEnhancers);

export default store;
