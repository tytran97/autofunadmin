import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import { ConnectedRouter } from "react-router-redux";
import { NotificationContainer } from "react-notifications";

import "polyfill";
import "bootstrap/dist/css/bootstrap.css";
import "./index.css";
import "../src/assets/css/style.css";

import registerServiceWorker from "./registerServiceWorker";
import store, { history } from "store";

import Login from "pages/Login";

import CarList from "pages/CarList";
import UserList from "pages/UserList";
import AcceList from "pages/AcceList";
import CarAdding from "pages/CarAdding";
import AcceAdding from "pages/AcceAdding";
import BillList from "pages/BillList";

const App = () => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div id="main-wrapper">
          <Route exact path="/login" component={Login} />
          <Route exact path="/" component={Login} />
          <Route exact path="/car/list" component={CarList} />
          <Route exact path="/user/list" component={UserList} />
          <Route exact path="/acce/list" component={AcceList} />
          <Route exact path="/car/create" component={CarAdding} />
          <Route exact path="/acce/create" component={AcceAdding} />
          <Route exact path="/bill/list" component={BillList} />
          <NotificationContainer />
        </div>
      </ConnectedRouter>
    </Provider>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
