// 開発環境用設定
const dev = {
  //BACKEND_URL: "https://dtc-lixil.firebaseapp.com/v1"
  BACKEND_URL: "http://localhost:8080"
};

//ステージング環境用設定

// const staging = {
//   BACKEND_URL: "https://lhtd-project-dev.firebaseapp.com"
// };

//本番環境用設定
const prod = {
  // BACKEND_URL: "https://lhtd-project.firebaseapp.com"
  //BACKEND_URL: "https://lhtd-api-project.firebaseapp.com/v1"
};

function chooseEnvironment(env) {
  switch (env) {
    case "development":
      return dev;
    case "production":
      return prod;
    default:
      return prod;
  }
}

const currentEnv = process.env.REACT_APP_STAGE || "development";
const config = chooseEnvironment(currentEnv);

export const BACKEND_URL = config.BACKEND_URL;

// 共通設定
export const DEFAULT_PAGINATION_LIMIT = 10;
