export const CarHeaders = [
  { value: "Xóa   Sửa", width: "100px" },
  { value: "Số thứ tự", width: "180px" },
  { value: "Tên xe" },
  { value: "Hộp số", width: "50%" },
  { value: "Năm sản xuất", width: "110px" },
  { value: "Giá (Triệu)", width: "100px" }
];

export const UserHeaders = [
  { value: "Xem", width: "180px" },
  { value: "Số thứ tự", width: "180px" },
  { value: "Tên đăng nhập", width: "110px" },
  { value: "Họ và tên" },
  { value: "Email", width: "50%" },
  { value: "Số điện thoại", width: "100px" }
];

export const AcceHeaders = [
  { value: "Xóa   Sửa", width: "100px" },
  { value: "Số thứ tự", width: "180px" },
  { value: "Tên sản phẩm", width: "110px" },
  { value: "Giá (Triệu)" },
  { value: "Số lượng", width: "50%" }
];

export const BillHeaders = [
  { value: "Xem", width: "100px" },
  { value: "Mã đơn hàng", width: "110px" },
  { value: "Trạng thái" },
  { value: "Người đặt hàng", width: "110px" },
  { value: "Duyệt đơn", width: "50%" }
];
